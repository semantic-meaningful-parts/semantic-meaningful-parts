fprintf('Starting up environment...\n');
fprintf('Suppressing Name Conflict warning...\n');
warning('off', 'MATLAB:dispatcher:nameConflict');

% Adding external_src
fprintf('Adding skeletonization toolbox setup...\n');
addpath(genpath('external_src/skeleton_telea'));
fprintf('Adding Fast Marching v3b toolbox...\n');
addpath(genpath('external_src/FastMarching_version3b'));
fprintf('Adding Piotr Dollar toolbox...\n');
addpath(genpath('external_src/toolbox_dollar'));
fprintf('Adding Piotr Dollar edge detector...\n');
addpath(genpath('external_src/edge_detector_dollar'));
fprintf('Adding fit ellipse toolbox...\n');
addpath(genpath('external_src/fit_ellipse'))
fprintf('Running VLFeat toolbox setup...\n');
addpath(genpath('external_src/vlfeat'));
fprintf('Adding Graph-Cuts GCO-v3.0 toolbox...\n');
addpath(genpath('external_src/gco-v3.0'));

% Add source code after in case fxn names overlap with external code
fprintf('Adding path for source code and data...\n');
addpath(genpath('src'));
addpath(genpath('src/_util'));
addpath(genpath('data'));
addpath(genpath('data/_util'));

% Set up the datasets
setup_data;

addpath('scratch');
