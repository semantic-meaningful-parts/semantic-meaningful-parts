function [semantic_parts, symmetry_axes] = plot_segment_struct(segment_struct, mask, opt)
  % if isempty(img); img = zeros(size(mask)); end;
  % gc_opts = struct; gc_opts.E = E;
  % if ~exist('opt', 'var') || isempty(opt)
  %   opt = struct; 
  %   opt.output = 'full'; 
  %   opt.gc_opts = struct; 
  % end
  % Generate the plot for semantic parts and symmetry axes
  semantic_parts = zeros(size(mask));
  num_parts = numel(segment_struct);
  segments = cell(1, num_parts);
  % Generate the semantic mask
  for part_id = 1:num_parts
    masters = segment_struct(part_id).master;
    peripherals = segment_struct(part_id).peripherals;
    segment = [];
    % Combine the master branches
    num_masters = numel(masters);
    for master_id = 1:num_masters
      master = masters{master_id};
      segment = [segment; master];
    end
    % Combine the peripheral branches
    num_peripherals = numel(peripherals);
    for peripheral_id = 1:num_peripherals
      peripheral = peripherals{peripheral_id};
      segment = [segment; peripheral];
    end
    segments{part_id} = segment;
  end
  % part_masks = assign2parts(segments, mask, 'full');
% <<<<<<< HEAD
%   [~, ~, semantic_parts] = assign2parts_gc(segments, mask, img, gc_opts); %
%   % [part_masks, pe, pm] = assign2parts_gc(segments, mask, img, gc_opts); %
%   % for part_id = 1:num_parts
%   %   mask_pixels = part_masks{part_id};
%   %   mask_pixels_ind = sub2ind(size(mask), mask_pixels(:, 1), mask_pixels(:, 2));
%   %   semantic_parts(mask_pixels_ind) = part_id;
%   % end
% =======
  part_masks = {segment_struct.patch};
  for part_id = 1:num_parts
    mask_pixels = part_masks{part_id};
    mask_pixels_ind = sub2ind(size(mask), mask_pixels(:, 1), mask_pixels(:, 2));
    semantic_parts(mask_pixels_ind) = part_id;
  end
% >>>>>>> 42de99266b0660afa4cd3ea0be4c3eaa034ce893
  % Generate the symmetry axes for each part
  symmetry_axes = plot_pixels(segments, mask, opt);
end
