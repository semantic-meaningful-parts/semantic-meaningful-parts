function S = sign_det_vectors(vector_AB, vector_AP)
  % Vector A contains two points (y_A, x_A; y_B, x_B)
  A = vector_AB(1, :);
  B = vector_AB(end, :);
  % Get the P component of vector_AP, for point P (y_P, x_P)
  P = vector_AP(end, :);
  % S = sign([(x_B-x_A)*(y_P-y_A)]-[(y_B-y_A)*(x_P-x_A)])
  S = sign(((B(2)-A(2))*(P(1)-A(1)))-((B(1)-A(1))*(P(2)-A(2))));
end


