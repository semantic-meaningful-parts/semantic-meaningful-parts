% decompose2segments
%
% @param skeleton symmetry axis in pixel space
% @return segments symmetry axis segments stored as a cell array
% 
% Takes the symmetry axis of an object and decomposes the axis into
% segments based on junctions.

function segments = decompose2segments(skeleton)
  NEIGHBORHOOD_SIZE = 3;
  INVALID_POINT = [-1, -1];
  % Skeletal segments as output
  segments = cell(1);
  % Branches and ends serve as endpoints for each part
  endpoints = bwmorph(skeleton, 'endpoints');
  branchpoints = bwmorph(skeleton, 'branchpoints');    
  % Set of terminal points is the union of branches and ends
  terminals = endpoints+branchpoints;
  terminals(terminals > 1) = 1;
  % Turn terminal pixels into indices
  [y_end, x_end] = find(endpoints == 1);
  yx_endpoint =[ y_end, x_end];
  [y_term, x_term] = find(terminals == 1);
  yx_terminals = [y_term, x_term];
  yx_terminals = combine_within_dist(yx_terminals, yx_endpoint, 3);
  num_terminals = size(yx_terminals, 1);
  % Turn skeleton into indices
  [y_skel, x_skel] = find(skeleton == 1);
  yx_skeleton = [y_skel, x_skel];
  % If the figure is a giant closed loop
  if num_terminals == 0
    segments{1} = yx_skeleton;
    return;
  end
  % Initialization for multiple parts in skeleton
  [y_max, x_max] = size(skeleton);
  curr_point = yx_terminals(1, :);
  curr_terminal = curr_point;
  segment = zeros(1, 2);
  segment_id = 1;
  curr_idx = 1;
  is_start = true;
  more_to_process = true;
  % Find skeletal segments until we exhaust skeleton
  while more_to_process
    segment(curr_idx, :) = curr_point;
    curr_idx = curr_idx+1;
    if is_in(curr_point, yx_terminals) & ~is_start
      segments{segment_id} = segment;
      % Increment segment id for next available segment
      segment_id = segment_id+1;
      % Reset the segment
      segment = zeros(1, 2);
      curr_idx = 1;
      is_start = true;
      segment(curr_idx, :) = curr_point;
      curr_idx = curr_idx+1;
      curr_terminal = curr_point;
    end % end-if(is_in(curr_point, yx_terminals) & ~is_start)
    % Allocate the set of neighborhood
    neighborhood = zeros(NEIGHBORHOOD_SIZE, NEIGHBORHOOD_SIZE);
    % Generate the set of neighboring range
    for y_neighbor_idx = 1:NEIGHBORHOOD_SIZE
      y_neighbor = curr_point(1)+y_neighbor_idx-2;
      % Check if we are within y-bounds
      if  y_neighbor <= y_max && y_neighbor > 0
        for x_neighbor_idx = 1:NEIGHBORHOOD_SIZE
          x_neighbor = curr_point(2)+x_neighbor_idx-2;
          % Skip if it is the current point or current terminal point
          if (y_neighbor == curr_point(1) && x_neighbor == curr_point(2)) ...
             || isequal(curr_terminal, [y_neighbor, x_neighbor])
            continue;
          end
          neighbor_value = 0;
          % Check if we are within x-bounds
          if x_neighbor <= x_max && x_neighbor > 0
            neighbor_value = skeleton(y_neighbor, x_neighbor); 
          end % end-if-(x_neighbor <= x_max && x_neighbor > 0)
          neighborhood(y_neighbor_idx, x_neighbor_idx) = neighbor_value;
        end % end-for-x_neighbor_idx
      end % end-if-(y_neighbor <= y_max && y_neighbor > 0)
    end % end-for-y_neighbor_idx
    % Get the neighboring pixels to curr_point
    [y_offset, x_offset] = find(neighborhood == 1);
    yx_offsets = [y_offset, x_offset];
    num_neighbors = numel(y_offset);
    if num_neighbors > 0
      neighbors = repmat(curr_point, num_neighbors, 1)+yx_offsets-2;
      % Check if any of the neighbors are terminal points
      found_next = false;
      for neighbor_idx = 1:num_neighbors
        neighbor = neighbors(neighbor_idx, :);
        % If this neighbor is a terminal point
        if is_in(neighbor, yx_terminals)
          % Set the current point that will be processed to it
          % Do not mark terminal point as 0 on skeleton 
          curr_point = neighbor;
          curr_terminal = curr_point;
          found_next = true;
        end % end-if-is_in(neighbor, yx_terminals)
      end % end-for-neighbor_idx
      % If we never found a terminal, then any point is good
      if ~found_next && num_neighbors > 0
        curr_point = neighbors(1, :);
        skeleton(curr_point(1), curr_point(2)) = 0;
      end
      % Whatever we found, we know we are not at start of new segment
      is_start = false;
    else % If still not found, then let's search globally
      if size(segment, 1) > 1
        segments{segment_id} = segment;
        % Increment segment id for next available segment
        segment_id = segment_id+1;
      end
      % Clear the pre-existing point
      segment = zeros(1, 2);
      curr_idx = 1;
      is_start = true;
      % Do not mark terminal point as 0 on skeleton
      curr_point = next_available_terminal(yx_terminals, skeleton);
      curr_terminal = curr_point;
    end

    % Check if we are able to find a point
    if isequal(curr_point, INVALID_POINT)
      more_to_process = false;
    end
  end % end-while-more_to_process
end % end-function

% Helper function
% Check to see if the point A is in the set B
function found = is_in(A, B)
  % Find locations where same values exists
  [a, ~] = find(A(:, 1) == B(:, 1));
  [b, ~] = find(A(:, 2) == B(:, 2));
  % Find the row of intersection
  i = intersect(a, b);
  found = false;
  % If intersection is equal 1 element, then A is in B
  if numel(i) == 1
      found = true; 
  end
end

% Helper function
% Next available terminal point
function next_terminal = next_available_terminal(yx_terminals, skeleton)
  NEIGHBORHOOD_SIZE = 3;
  [y_max, x_max] = size(skeleton);
  num_terminals = size(yx_terminals, 1);
  next_terminal = [-1, -1];
  for terminal_idx = 1:num_terminals
    curr_terminal = yx_terminals(terminal_idx, :);
    % Allocate the set of neighborhood
    neighborhood = zeros(NEIGHBORHOOD_SIZE, NEIGHBORHOOD_SIZE);
    % Generate the set of neighboring range
    for y_neighbor_idx = 1:NEIGHBORHOOD_SIZE
      y_neighbor = curr_terminal(1)+y_neighbor_idx-2;
      % Check if we are within y-bounds
      if  y_neighbor <= y_max && y_neighbor > 0
        for x_neighbor_idx = 1:NEIGHBORHOOD_SIZE
          x_neighbor = curr_terminal(2)+x_neighbor_idx-2;
          % Skip if it is the current point we are processing
          if y_neighbor == curr_terminal(1) && x_neighbor == curr_terminal(2)
            continue;
          end
          neighbor_value = 0;
          % Check if we are within x-bounds
          if x_neighbor <= x_max && x_neighbor > 0
            neighbor_value = skeleton(y_neighbor, x_neighbor); 
          end % end-if-(x_neighbor <= x_max && x_neighbor > 0)
          neighborhood(y_neighbor_idx, x_neighbor_idx) = neighbor_value;
        end % end-for-x_neighbor_idx
      end % end-if-(y_neighbor <= y_max && y_neighbor > 0)
    end % end-for-y_neighbor_idx
    % Get the neighboring pixels to curr_point
    [y_offset, x_offset] = find(neighborhood == 1);
    yx_offset = [y_offset, x_offset];
    num_neighbors = numel(y_offset);
    if num_neighbors > 0
      next_terminal = curr_terminal;
      return;
    end
  end % end-terminal_idx
end

% Helper function
% Simple clustering method that joins two points if they are close
function combined = combine_within_dist(points, preferred, min_dist)
  num_points = size(points, 1);
  rejected = [];
  for curr_idx = 1:num_points
    if numel(find(rejected == curr_idx)) > 0
      continue;
    end % end-if-(numel(find(rejected == curr_idx)) > 0)
    curr_point = points(curr_idx, :);
    for next_idx = 1:num_points
      % Skip if it is the same point
      if curr_idx == next_idx
        continue;
      end % end-if-(curr_idx == next_idx)
      if numel(find(rejected == next_idx)) > 0
        continue;
      end % end-if-(numel(find(rejected == next_idx)) > 0)
      % Get the next point
      next_point = points(next_idx, :);
      % If too close together
      if pdist([curr_point; next_point], 'euclidean') < min_dist
        curr_preferred = is_in(curr_point, preferred);
        next_preferred = is_in(next_point, preferred);
        if ~next_preferred
          rejected = [rejected, next_idx];
        elseif curr_preferred && next_preferred
          rejected = [rejected, next_idx];
        end % end-if-curr_preferred-next_preferred
      end % end-if-(pdist([curr_point; next_point], 'euclidean') < min_dist)
    end % end-for-next_idx
  end % end-for-curr_idx
  combined = points(setdiff(1:num_points, rejected), :);
end % end-function

