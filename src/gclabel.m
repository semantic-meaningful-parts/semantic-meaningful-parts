%---------------------------------------------------------------------------
% gclabel 
%
% runs multilabel graph cuts on input ellipses plot and mask
% 
% @param: render: ellipse model rendered at this angle
% 
%---------------------------------------------------------------------------
function map = gclabel(render, mask, img, opts)

% parameters
CONSTANT = 1;
UNARY = 100;
PAIR = 10;
BOOST = 10;

mask = mask > 0.5;

[rows,cols] = size(mask); imsize = [rows, cols]; npx = rows*cols; 
z = zeros(imsize);
[~, ~, dxi, dyi] = make_difference_operator(imsize);

% pixelweights computation parameters
if ~exist('opts', 'var'); opts = struct; end;
% if ~isfield(opts, 'alpha');  opts.relative_weights = [0.5, 0.0, 0.0]; end;
if ~isfield(opts, 'alpha');  opts.relative_weights = [0.5, 0.0, 0.5]; end;
if ~isfield(opts, 'SIGMOID_MEAN'); opts.SIGMOID_MEAN = 0.55; end; 
if ~isfield(opts, 'SIGMOID_SCALE'); opts.SIGMOID_SCALE = 100; end;
if ~isfield(opts, 'WEIGHTS_LOW_CUTOFF'); opts.WEIGHTS_LOW_CUTOFF = 0.1; end;
% if ~isfield(opts, 'patch_mask'); opts.patch_mask = -ones(imsize); end;
% if ~isfield(opts, 'emodel'); A = load('modelFinal.mat'); opts.emodel = A.model; end;

%---------------------------------------------------------------------
% unary
%---------------------------------------------------------------------  
% labels = unique(render); num_parts = length(labels); nl = num_parts+1; 
% pmask = double(mask)*(1/nl);
% pmask = double(mask)*0.5;

labels = unique(render); 
labels(labels<1) = []; % removes label 0 and 0.5 (for rendered boundaries ty render model)
num_parts = length(labels); nl = num_parts+1; 
[pfg_all, labels] = explode(render, labels);

punary = max(0, cat(3, nl*mask, 1.0 - (bsxfun(@plus, 0.5*pfg_all, 0.5*double(mask)))));

unary = reshape(punary, npx, nl)';

%---------------------------------------------------------------------
% compute pairwise pixel weights
%---------------------------------------------------------------------
% gpb = edgesDetect(img, opts.edge_model);
gpb = opts.E;
ed = 0.75 * (1.0 - gpb); % ed = [];
[wall, wimg, wuv, we] = make_weights(img, [], [], dxi, dyi, ed, opts);
pxl_weights = 1.0 - wall;
% sweights = sigmoid(pxl_weights, opts.SIGMOID_MEAN, opts.SIGMOID_SCALE);
% fig; imagex([yw_img; yw_gpb; yw_uv; bw_uv]);
weights = pxl_weights;

% weights = ones(rows, cols, 2);
pair = sparse([dxi(:,1); dyi(:,1)], [dxi(:,2); dyi(:,2)], ...
  double(weights(:)), npx, npx);

%---------------------------------------------------------------------
% do optimization
%---------------------------------------------------------------------
h = GCO_Create(npx, nl); % global opt for now
GCO_SetDataCost(h, double(CONSTANT * UNARY * unary)); % unary / similarity scores
GCO_SetSmoothCost(h, double(1 - eye(nl))); % pott's model on label values
GCO_SetNeighbors(h, double(CONSTANT * PAIR * pair));

% solve problem
% GCO_SetLabeling(h, int32(init_labels(:)));
% GCO_SetVerbosity(h, 2);
gco_maxiters = max(rows, cols)*10;
GCO_Expansion(h, gco_maxiters);
L = GCO_GetLabeling(h);
% [E, D, S] = GCO_ComputeEnergy(h);

map = reshape(L, rows, cols)-1;
% [pxls_part, pxls_edge] = map2fmt(map, dxi, dyi);
end


%---------------------------------------------------------------------
% this changes the map to the alex's format
%---------------------------------------------------------------------
function [pxls_part, pxls_edge] = map2fmt(map, dxi, dyi)
[rows,cols] = size(map); npx = rows*cols; pxls = 1:npx;
labels = unique(map);
labels(1) = []; % assume and remove label 0 (background)
num_parts = length(labels);
pxls_parts = {};

mask = map>0;
dx = mask(dxi(:,1))-mask(dxi(:,2));
dy = mask(dyi(:,1))-mask(dyi(:,2));
edge_map = zeros(rows, cols);

idx_dx = (dx>0).*dxi(:,1) + (dx<0).*dxi(:,2); idx_dx(idx_dx == 0) = [];
idx_dy = (dy>0).*dyi(:,1) + (dy<0).*dyi(:,2); idx_dy(idx_dy == 0) = [];

edge_map(idx_dx) = map(idx_dx);
edge_map(idx_dy) = map(idx_dy);

% edge_pxls = cell(4,1);
% edge_pxls{1} = dxi(:,1);
% edge_pxls{2} = dxi(:,2);
% edge_pxls{3} = dyi(:,1);
% edge_pxls{4} = dyi(:,2);
% k = 1;
% edge_map = map(edge_pxls{k});
% map0 = edge_map == 0;
% while sum(sum(map0))>0 && k < 4;
%   k=k+1;
%   edge_map_k = map(edge_pxls{k});
%   edge_map(map0) = edge_map_k(map0);
%   map0 = edge_map == 0;
% end

% emsk = edge(map>0, 'sobel');
% emsk2 = [[emsk(2:end,2:end), emsk(2:end,1)]; [emsk(1,2:end), emsk(1,1)]];
% edge_map = map(emsk); % TODO: check on this
% edge_map2 = map(emsk2); % TODO: check on this
% idx0 = edge_map == 0;
% keyboard;
% edge_map(idx0) = edge_map2(idx0);

for k = 1:num_parts;
  [y,x] = ind2sub([rows, cols], pxls(map(:) == k));
  pxls_part{k} = [y,x];
  [y,x] = ind2sub([rows, cols], pxls(edge_map(:) == k));
  pxls_edge{k} = [y,x];
end
end
