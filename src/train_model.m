function models0_poses = train_model(imgs)
  num_imgs = numel(imgs);
  for img_id = 1:num_imgs
    img = imgs{img_id};
    img_labels = get_labels(img);
    % Generate ellipses for each label
    num_labels = numel(img_labels);
    ellipse_models = cell(1, num_labels);
    for label_id = 1:num_labels
      yx_label = img_labels{label_id};
      ellipse_models{label_id} = fit_ellipse_model(yx_label, size(img), 3);
    end
    % Find the center/'torso' of the object
    G = to_adj_graph(img, 3, 'masks');
    [max_flow_node, io_paths, verbose_paths] = max_flow_adj(G);
    torso_model = ellipse_models{max_flow_node};
    norm_scale = 1.0/torso_model.long_axis;
    num_parts = numel(ellipse_models);
    % Normalize the size of each part with respect to the torso
    norm_models = cell(1, num_parts); 
    for part_id = 1:num_parts
      part_model = ellipse_models{part_id};
      norm_models{part_id} = norm3d(part_model, torso_model, norm_scale, size(img));
    end % end-for-part_id
    % Retain original torso and all parts is with respect to original torso
    norm_models{max_flow_node}.a = torso_model.a;
    norm_models{max_flow_node}.long_axis = torso_model.long_axis;
    norm_models{max_flow_node}.X0_in = torso_model.X0_in;
    norm_models{max_flow_node}.Y0_in = torso_model.Y0_in;
    norm_models{max_flow_node}.phi = torso_model.phi;
    norm_models{max_flow_node}.dims = size(img);
    norm_models{max_flow_node}.tight_dims = size(tighten_canvas(img));
    % Updating the initial model
    if img_id == 1 % If this is the first image, use this as initialization
      num_rots = 16;
      models0_poses = cell(1, num_rots);
      models0_poses{1} = norm_models; % index 1 refers to 0 degrees
    else % Insert the learned configuration in the appropriate slot
      figure; imagesc(img)
      pose_models = update_models(models0_poses, norm_models, max_flow_node, img);
      [pose_models.deg_rotate, pose_models.deg_id]
      models0_poses{pose_models.deg_id} = pose_models.models; % Update the pose
    end
  end % end-for-img_id
end

function pose_models = update_models(models0_poses, models1, center_id, img)
  % Estimate the approximate pose of the models1
  [pose_models, id_mappings] = approx_pose(models0_poses, models1, img, 16, center_id, 2);
  % For each of the ellipses, change their parameters to fit models1
  orient_models = pose_models.models;
  target_models = pose_models.targets;
  dims = size(img);
  z = zeros(dims);
  target_render = render_model(target_models, z, center_id);
  orient_render = render_model(orient_models, z, center_id);
  figure; subplot(1, 3, 1); imagesc(target_render) 
  subplot(1, 3, 2); imagesc(orient_render)
  num_models = numel(orient_models);
  for model_id = 1:num_models
    src_models = orient_models{model_id};
    tgt_models = target_models{model_id};
    num_src_parts = numel(src_models);
    num_tgt_parts = numel(tgt_models);
    % If the target provides more information or are equally good
    if num_src_parts <= num_tgt_parts
      % Let's go ahead and trust it to improve our model
      for ell_id = 1:num_tgt_parts
        tgt_models(ell_id).Z1 = src_models(1).Z1;
      end
      orient_models{model_id} = tgt_models;
    elseif num_src_parts > num_tgt_parts % If our model is more sophisticated
      % Let's adjust our parameters such that it fits well to the target
      sum_long = sum([src_models.long]);
      pct_long = [src_models.long]/sum_long;
      d_long = (sum([tgt_models.long])-sum_long)/sum_long;
      upd_long = 1+pct_long*d_long;
      sum_short = sum([src_models.short]);
      pct_short = [src_models.short]/sum_short;
      d_short = (sum([tgt_models.short])-sum_short)/sum_short;
      upd_short = 1+pct_short*d_short;
      % Apply the changes to the long, short axes and phi
      for ell_id = 1:num_src_parts
      % [src_models(ell_id).long, src_models(ell_id).short, src_models(ell_id).phi1]
        src_models(ell_id).long = src_models(ell_id).long*upd_long(ell_id);
        src_models(ell_id).short = src_models(ell_id).short*upd_short(ell_id);
        src_models(ell_id).phi1 = tgt_models(1).phi1;
      % [src_models(ell_id).long, src_models(ell_id).short, src_models(ell_id).phi1]
      end % end-for-ell_id
      orient_models{model_id} = src_models;
    end % end-if-num_src_parts
  end % end-for-model_id
  pose_models.models = orient_models;
  orient_render = render_model(orient_models, z, center_id);
  subplot(1, 3, 3); imagesc(orient_render)
end

















