% tighten_canvas
%
% @param img N by M matrix
% @return tightened tight image of img
% 
% Takes an image and computes the bounding box around the object
% and crops the image to give the tightened image.

function tightened = tighten_canvas(img) 
    % Get 'high' pixels in the image
    [r, c] = find(img > 0);
    % Generate a bounding box around the pixels
    maxr = max(r);
    minr = min(r);
    maxc = max(c);
    minc = min(c);

    tightened = img(minr:maxr, minc:maxc);
end