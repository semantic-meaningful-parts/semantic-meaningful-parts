% get_relative_part
%
% @param part pixel coordinates
% @return relative_part pixel coordinates centered at center of image
%
% Takes a set of pixel coordinates and centers them at the center
% of the image by computing an offset based on coordinates span.

function relative_part = get_relative_part(part) 
  offset = get_relative_offset(part);
  relative_part = bsxfun(@minus, part, offset);
end