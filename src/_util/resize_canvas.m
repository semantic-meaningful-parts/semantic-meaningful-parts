% resize_canvas
% 
% @param img N by M matrix
% @param height the final height we want for the canvas
% @param width the final width we want for the canvas
% @return resized a height by width matrix
%
% Takes an image and resizes the canvas to the given height and width
% If the image is larger than the given height and width, we will
% resize the image and generate a canvas that fits the given

function resized = resize_canvas(img, height, width)
    
    [num_rows, num_cols] = size(img);
    % Do nothing if it already is the same size
    if num_rows == height && num_cols == width
        resized = img;
        return;
    end
    % Adjust 
    if num_rows > height && num_rows >= num_cols
        img = imresize(img, [height, NaN]);
    elseif num_cols > width && num_cols >= num_rows
        img = imresize(img, [NaN, width]);
    end
    
    [num_rows, num_cols] = size(img);
    % Take the difference between the given and the current rows and cols
    diff_rows = height-num_rows;
    diff_cols = width-num_cols;
    
    resized = img;
    % If we need more cols
    if diff_cols > 0
        % Append the same number of cols on either side
        cols_to_append = zeros(num_rows, floor(diff_cols/2));
        resized = [cols_to_append, resized];
        resized = [resized, cols_to_append];
        % If we are off by 1, then append one more
        if size(resized, 2) < width
            resized = [resized, zeros(num_rows, 1)];
        end
    end
    % If we need more rows
    if diff_rows > 0
        % Append the same number of rows on either side
        rows_to_append = zeros(floor(diff_rows/2), width);
        resized = [rows_to_append; resized];
        resized = [resized; rows_to_append];
        % If we are off by 1, then append one more
        if size(resized, 1) < height
            resized = [resized; zeros(1, width)];
        end
    end
end