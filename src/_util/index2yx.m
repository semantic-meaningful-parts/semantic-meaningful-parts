function coordinates = index2yx(coordinate_plane, index)
  params = get_params('utility');
  % Coordinate plane: [y_min, x_min; y_max, x_max] 
  yx_min = coordinate_plane(1, :);
  yx_max = coordinate_plane(2, :);
  % Number of rows and columns
  y_span = yx_max(params.y)-yx_min(params.y)+1;
  x_span = yx_max(params.x)-yx_min(params.x)+1;
  % Given that our index is on a [1, 1; n, m] plane
  [y_shifted, x_shifted] = ind2sub([y_span, x_span], index);
  % Compute the offset that shifts our y and x
  y_offset = 1-yx_min(params.y);
  x_offset = 1-yx_min(params.x);
  % Offset the shift to return to our original plane
  coordinates = [y_shifted-y_offset, x_shifted-x_offset];
end