function img = plot_pixels(pixel_groups, mask, opt)
  if ~exist('opt', 'var') || isempty(opt)
    opt = '';
  end
  num_groups = numel(pixel_groups);
  img = zeros(size(mask));
  mask(mask > 1) = 1;
  for group_id = 1:num_groups
    pixels = pixel_groups{group_id};
    if ~isempty(pixels)
      pixels_ind = sub2ind(size(mask), pixels(:, 1), pixels(:, 2));
      img(pixels_ind) = group_id;
    end
  end
  if strcmp(opt, 'overlay')
    img = double(img)+double(mask);
  end
end