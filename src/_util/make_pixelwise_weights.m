%-----------------------------------------------------------------------------
% make_pixelwise_weights
%
% @return: constraints_: constraints post warping forward in time
% @return: w_uv: associated weights post warping (larger warp -> larger decay)
% @return: valid: which boxes are valid and which are not
% @param: constraints: constraints in prior frame to warp forward
% @param: uvb_rev (MxNx2): warping to use to propagate constraints from prior 
%   frame to current one
% @param: SAFESPEEDSQUARED: parameter that affects weight decay w.r.t. warp
%-----------------------------------------------------------------------------
function [w, w_img, w_uv, w_gpb] = make_pixelwise_weights( ...
  idx, I_, uvb_, uvf_, gpb, weights, opts)

%-----------------------------------------------------------------------------
% settings 
%-----------------------------------------------------------------------------
COMPUTE_EDGES_UV = true;
WEIGHT_MAG_THRESHOLD = 0.65;
THETA = 0.2;
MINWUV = 5e-3;
VIS = 9000;
v2struct(opts);

weights = weights/sum(weights);

[rows, cols, chan] = size(I_);
imsize = [rows, cols]; uvsize = [rows, cols, 2];
n = rows * cols; nw = n*2;

%--------------------------------------------------------------------
% meat
%--------------------------------------------------------------------
I = reshape(I_, [n, chan]); % TODO: check this
uvf_(isnan(uvf_) | isinf(uvf_)) = 0.0;
uvb_(isnan(uvb_) | isinf(uvb_)) = 0.0;
uvf = reshape(uvf_, [numel(uvf_)/2, 2]);
uvb = reshape(uvb_, [numel(uvb_)/2, 2]);
idx1 = idx(:,1);
idx2 = idx(:,2);

% rgb intensity weight
if weights(1)>0
  d_img = sum( (I(idx1,:) - I(idx2,:)).^2, 2);
  w_img = exp( -d_img/mean(d_img) );
else
  w_img = zeros( length(idx1), 1);
end

% motion weight
if weights(2)>0
  w_uvf = sum( (uvf(idx1,:) - uvf(idx2,:)).^2, 2);
  w_uvb = sum( (uvb(idx1,:) - uvb(idx2,:)).^2, 2);

  % flow weight (forward)
  mean_w_uvf = mean(w_uvf); fprintf('mean uvf: %0.3f\n', mean_w_uvf);
  w_uvf_ = exp( -w_uvf/ max(MINWUV, mean(w_uvf)) );
  
  % flow weight (backward)
  mean_w_uvb = mean(w_uvb); fprintf('mean uvb: %0.3f\n', mean_w_uvb);
  w_uvb_ = exp( -w_uvb/ max(MINWUV, mean(w_uvb)) );
  
  w_mag = 0.5*(w_uvf_+w_uvb_);
  w_uv = w_mag;

  % compute angle weights
  df = flow_angle_distance_mex( double(uvf_) );
  db = flow_angle_distance_mex( double(uvb_) );
  theta_dist = (df + db)/2;
  w_theta = exp( -theta_dist(:)/THETA ); 
  w_theta = repmat(w_theta,[2,1]);
  % w_uv0 = reshape(w_uv, uvsize);
  w_uv = w_mag ...
    .* ( w_theta.*(w_mag <= WEIGHT_MAG_THRESHOLD) + (w_mag > WEIGHT_MAG_THRESHOLD) );
  
  if COMPUTE_EDGES_UV;
    uvf_mag = sqrt(sum(uvf_.^2, 3)) + 1e-8; uvf_mmax = max(abs(uvf_mag(:)));
    uvb_mag = sqrt(sum(uvb_.^2, 3)) + 1e-8; uvb_mmax = max(abs(uvb_mag(:)));
    uvf_img = cat(3, 0.5*(1.0+bsxfun(@rdivide,uvf_,uvf_mag)), uvf_mag/uvf_mmax);
    uvb_img = cat(3, 0.5*(1.0+bsxfun(@rdivide,uvb_,uvb_mag)), uvb_mag/uvb_mmax);
    try
      we_uvf = edgesDetect(uvf_img, opts.edge_model.model);
      we_uvb = edgesDetect(uvb_img, opts.edge_model.model);
      we_uv_ = 0.5*(0.75*(1.0 - we_uvf) + 0.75*(1.0 - we_uvb));
      we_uv = we_uv_(idx1);      
    catch e; we_uv = w_uv; keyboard; display(e); end
  end
else
  we_uv = zeros( length(idx1), 1);
  w_uv = zeros( length(idx1), 1);        
end

% gpb weight:
if weights(3) > 0;
  if isempty(gpb);
    gpb = 1.0 - edgesDetect(I_, opts.edge_model.model);
  end
  gpb = gpb(:);
  try
  w_gpb = gpb(idx1);
  catch e; display(e); keyboard; end;
else
  w_gpb = zeros( length(idx1), 1);
end

% fixing last column and row first
% w_gpb(:,end,:) = w_gpb(:,end-1,:);

% combinations
% weights = bxsfun(weights ./ repmat(sum(weights, 2), 1, 3);
weights = bsxfun(@rdivide,weights,sum(weights,2));
w = w_img .* (weights(:,1)) ...
  + w_uv  .* (weights(:,2)) ...
  + w_gpb .* (weights(:,3));

if VIS < 150;
  % debugging visuals if need be
  vis_w_img = reshape(min(w_img(1:n), w_img(n+1:2*n)), imsize);
  vis_w_uv  = reshape(min(w_uv(1:n) , w_uv(n+1:2*n)) , imsize);
  vis_w_gpb = reshape(min(w_gpb(1:n), w_gpb(n+1:2*n)), imsize);
  vis_w     = reshape(min(w(1:n)    , w(n+1:2*n))    , imsize);

  fig(2002); clf;
  vl_tightsubplot(2,3,1); imagesc(vis_w_img); notick; title('lab');
  vl_tightsubplot(2,3,2); imagesc(vis_w_uv); notick; title('uv');
  vl_tightsubplot(2,3,3); imagesc(vis_w_gpb); notick; title('gpb');

  vl_tightsubplot(2,3,6); imagesc(vis_w); notick;
  vl_tightsubplot(2,3,5);
end

% OUTPUT
w     = fix_w(reshape(w, [rows, cols, 2]));
w_img = fix_w(reshape(w_img, [rows, cols, 2]));
w_uv  = fix_w(reshape(w_uv, [rows, cols, 2]));
w_gpb = reshape(w_gpb, [rows, cols, 2]);
we_uv = fix_w(reshape(we_uv, [rows, cols, 2]));
end

% small fixing fxn
function w = fix_w(w)
w(:,end,1) = w(:,end-1,1);
w(end,:,2) = w(end-1,:,2);
end
