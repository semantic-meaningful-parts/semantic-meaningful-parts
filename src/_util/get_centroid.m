function centroid = get_centroid(img)
  labels = bwlabel(img);
  prop_struct = regionprops(labels, 'centroid');
  centroid = prop_struct.Centroid;
  centroid = [centroid(2), centroid(1)];
end