%-----------------------------------------------------------------------------
% cvos weights: computes weights step for framework
%
% @param: I1 : image to compute weights from
% @param: ...
% @param: opts : containts following
%   * edge_model
%   * SIGMOID_MEAN
%   * SIGMOID_SCALE
%   * relative_weights
%-----------------------------------------------------------------------------
function [weights, w_img, w_uv, w_gpb] = make_weights(Ilab, uvb, uvf, ...
  dx_inds, dy_inds, E, opts)
v2struct(opts);

[weights, w_img, w_uv, w_gpb] = make_pixelwise_weights([dx_inds; dy_inds], ...
  Ilab, uvb, uvf, E, relative_weights, opts);

% weights_sigmoid = sigmoid(weights, SIGMOID_MEAN, SIGMOID_SCALE);
% weights1 = weights_sigmoid;
% weights1( weights1 < WEIGHTS_LOW_CUTOFF ) = 0;

weights = 1 - weights;
end
