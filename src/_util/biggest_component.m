% returns the mask of the largest component and the whole cc in the second output
function [big, cc_info] = biggest_component(msk)
msk = msk > 0.5;
cc_info = bwconncomp(msk);
if cc_info.NumObjects == 1;
  big = msk; cc = msk;
else
  szs = arrayfun(@(x) size(x{1},1), cc_info.PixelIdxList);
  cc_info.szs = szs;
  [~,idx_biggest] = max(szs);
  big = zeros(cc_info.ImageSize);
  big(cc_info.PixelIdxList{idx_biggest}) = 1;
end
end
