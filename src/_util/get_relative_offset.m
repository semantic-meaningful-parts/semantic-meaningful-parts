% get_relative_offset
%
% @param part pixel coordinates
% @return offset yx-offset to shift pixels to center at center of image 
%
% Takes a set of pixel coordinates and computes the offsets that is needed
% to center the pixels at the center of an image.

function offset = get_relative_offset(part)
  y_min = min(part(:,1)); 
  y_max = max(part(:,1));
  x_min = min(part(:,2)); 
  x_max = max(part(:,2));
  % Center with respect to origin (0,0) at image center
  y_offset = ceil((y_min+y_max)/2);
  x_offset = ceil((x_min+x_max)/2);
  offset = [y_offset, x_offset];
end