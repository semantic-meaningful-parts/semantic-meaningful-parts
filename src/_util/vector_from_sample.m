function vector_A = vector_from_sample(point_A, map_A, SAMPLE_SIZE)
  params = get_params('default');
  orig_A = point_A;
  continue_A = true;
  for sample_id = 1:SAMPLE_SIZE
    % Follow the singly connected path of contour map of A from the shared pixel
    if continue_A
      % Look around the current pixel for the next pixel
      neighborhood_A = map_A(point_A(params.y)-1:point_A(params.y)+1, ...
                             point_A(params.x)-1:point_A(params.x)+1);
      [y_A, x_A] = find(neighborhood_A == 1); 
      if size(y_A, 1) > 0
        point_A = [point_A(params.y)+y_A(params.first)-2, ...
                   point_A(params.x)+x_A(params.first)-2];
        map_A(point_A(params.y), point_A(params.x)) = ...
            map_A(point_A(params.y), point_A(params.x))+2; 
      else
        continue_A = false;
      end
    end % end-if-continue_A
  end % end-for-sample_id
  % Form vector A
  vector_A = [point_A; orig_A];
end