% get_relative_junctions
%
% @param part pixel coordinates
% @param junctions junctions of the part
% @return relative_junctions junctions shifted by offset 
%
% Takes a set of pixel coordinates and their junctions and centers
% the junctions based on the offset computed from the pixel coordinates

function relative_junctions = get_relative_junctions(junctions, part, yx_dim) 
  params = get_params('utility');
  relative_junctions = containers.Map('KeyType', 'double', 'ValueType', 'any');
  % Get offset base on part
  offset = get_relative_offset(part);
  keys = cell2mat(junctions.keys);
  num_keys = numel(keys);
  % If there are no junctions
  if num_keys == 0
    return;
  end
  % Offset the keys to get relative keys
  y_min = min(part(:,1)); 
  y_max = max(part(:,1));
  x_min = min(part(:,2)); 
  x_max = max(part(:,2));
  coordinate_keys = index2yx([1, 1; yx_dim], keys');
  relative_yxs = bsxfun(@minus, coordinate_keys, offset);
  for key_idx = 1:num_keys
    key_original = keys(key_idx);
    value = junctions(key_original);
    relative_yx = relative_yxs(key_idx, :);
    yx_min = [y_min, x_min];
    yx_max = [y_max, x_max];
    relative_key = yx2index([yx_min-offset; yx_max-offset], relative_yx);
    relative_junctions(relative_key) = value;
  end
end