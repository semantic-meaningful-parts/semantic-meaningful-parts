% Helper function
% Sorts the segments 
function segment_struct = sort_segment_struct(segment_struct, opt)
  if ~exist('opt', 'var') || isempty(opt)
    opt = 'out_in'
  end
  opt = lower(opt);
  num_segments = numel(segment_struct);
  terminals = [segment_struct.is_terminal];
  % First find the set of terminal ids
  terminal_ids = find(terminals == true);
  general_ids = setdiff(1:num_segments, terminal_ids);
  % Parition our segments and complexities into terminal and genearl set
  terminal_segments = segment_struct(terminal_ids);
  general_segments = segment_struct(general_ids);
  if strcmp(opt, 'out_in')
    % Sort by merging the sets such that we process terminals first
    segment_struct = [terminal_segments, general_segments];
  elseif strcmp(opt, 'in_out')
    % Sort by merging the sets such that we process non-terminals first
    segment_struct = [general_segments, terminal_segments];
  end
end % end-function
