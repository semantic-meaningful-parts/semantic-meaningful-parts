function [vector_A, segment_A] = segment_vector(junction, segments, SAMPLE_SIZE)
  num_segments = numel(segments);
  segment_A = [];
  for segment_id = 1:num_segments
    segment = segments{segment_id};
    SAMPLE_SIZE = min(size(segment, 1)-1, SAMPLE_SIZE);
    if pdist([junction; segment(end, :)], 'euclidean') < 3 
      vector_A = [segment(end-SAMPLE_SIZE, :); junction];
      segment_A = segment;
    else
      vector_A = [segment(SAMPLE_SIZE, :); junction];
      segment_A = flipud(segment);
    end
  end
end