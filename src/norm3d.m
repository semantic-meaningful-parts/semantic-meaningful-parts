function model3d = norm3d(part_model, torso_model, norm_scale, dims)
  num_ellipses = numel(part_model);
  for e_id = 1:num_ellipses
    % Determine which which axis (yx) is the major axis
    if abs(part_model(e_id).long_axis-2*part_model(e_id).a) < ...
       abs(part_model(e_id).long_axis-2*part_model(e_id).b)
      model3d(e_id).axis_flip = false;
    else
      model3d(e_id).axis_flip = true;
    end
    model3d(e_id).X1 = norm_scale*(part_model(e_id).X0_in-torso_model.X0_in);
    model3d(e_id).Y1 = norm_scale*(part_model(e_id).Y0_in-torso_model.Y0_in);
    model3d(e_id).Z1 = 0;
    model3d(e_id).long = norm_scale*part_model(e_id).long_axis;
    model3d(e_id).short = norm_scale*part_model(e_id).short_axis;
    phi0 = part_model(e_id).phi;
    if model3d(e_id).axis_flip
      phi0 = phi0-pi/2; 
    end;
    model3d(e_id).phi1 = mod(rad2deg(phi0-torso_model.phi), 360);
    model3d(e_id).Zcam = norm_scale*floor(dims(2)/2);
    model3d(e_id).X2 = model3d(e_id).X1+model3d(e_id).long*cos(phi0);
    model3d(e_id).Y2 = model3d(e_id).Y1+model3d(e_id).long*sin(phi0);
    model3d(e_id).Z2 = 0;
    %%%% TODO: USE 3RD point so that we can maintain thickness of rotates 
    %%%%       about y axis along long axis (in direction of short axis)
    model3d(e_id).X3 = model3d(e_id).X1+model3d(e_id).short*sin(phi0);
    model3d(e_id).Y3 = model3d(e_id).Y1+model3d(e_id).short*cos(phi0);
    model3d(e_id).Z3 = 0;
  end % end-for-e_id
end % end-function
