function [pose_models, id_mappings] = approx_pose(models0_poses, models1, img, num_rots, center, opt)
  % models0 is the original model learned from the first sample
  models0 = models0_poses{1};
  dims = size(img);
  % Output structure
  pose_models = struct('models', [], ...
                       'targets', [], ...
                       'dims', dims, ...
                       'center_id', center, ...
                       'a', models0{center}.a, ...
                       'long_axis', models0{center}.long_axis, ...
                       'Y0', models0{center}.Y0_in, ...
                       'X0', models0{center}.X0_in, ...
                       'deg_rotate', 0, ...
                       'deg_id', 0);
  % Get the rotated model at a given degree
  nrow = num_rots/2;
  models0_rot = cell(1, num_rots);
  deg_mat = zeros(1, num_rots);
  z = zeros(dims);
  figure;
  % Make everything with respect to models1
  models0{center}.a = models1{center}.a;
  models0{center}.long_axis = models1{center}.long_axis;
  % Use model1's ellipse center
  models0{center}.X0_in = models1{center}.X0_in;
  models0{center}.Y0_in = models1{center}.Y0_in;
  for kr = 0:num_rots-1;
    % If we do not have a pose estimated for 
    if isempty(models0_poses{kr+1})
      deg = floor((kr/num_rots)*360);
      deg_mat(kr+1) = deg;
      mrotate = deg2rotmatrix(deg);
      model_rot = xform_model(models0, center, mrotate);
      o = render_model(model_rot, z, center);
      subplot(2,nrow,kr+1); imagesc(o); title(deg);
      % Store the rotated models
      models0_rot{kr+1} = model_rot;
    else
      deg = floor((kr/num_rots)*360);
      deg_mat(kr+1) = deg;
      model_rot = models0_poses{kr+1};
      % Make everything with respect to models1
      model_rot{center}.a = models1{center}.a;
      model_rot{center}.long_axis = models1{center}.long_axis;
      % Use model1's ellipse center
      model_rot{center}.X0_in = models1{center}.X0_in;
      model_rot{center}.Y0_in = models1{center}.Y0_in;
      mrotate = deg2rotmatrix(0);
      model_rot = xform_model(model_rot, center, mrotate);
      o = render_model(model_rot, z, center);
      subplot(2,nrow,kr+1); imagesc(o); title(deg);
      models0_rot{kr+1} = model_rot;
    end
  end
  % Generate the unrotated models1 in 3D
  mrotate = deg2rotmatrix(0);
  models1_rot = xform_model(models1, center, mrotate);
  % Precompute the centroids of models1
  YX1 = zeros(numel(models1_rot), 2);
  for m1_id = 1:numel(models1_rot)
    model1 = models1_rot{m1_id};
    y1 = mean([model1.y1]);
    x1 = mean([model1.x1]);
    YX1(m1_id, :) = [y1, x1];
  end
  kd_tree_YX1 = vl_kdtreebuild(YX1');
  % If we are not given the head, search over all parts
  if ~exist('opt', 'var') || isempty(opt)
    num_k = 5;
    % Find the distance of head to closest part centroid
    dist_mat = zeros(1, num_rots);
    for r = 1:num_rots
      model0_rot = models0_rot{r};
      head_model0 = model0_rot{1};
      YX0_head = [head_model0.y1, head_model0.x1];
      [~, dist] = vl_kdtreequery(kd_tree_YX1, YX1', YX0_head');
      dist_mat(r) = sqrt(dist);
    end
    % Find top 5 closest part fits to the model head position
    [sorted, order] = sort(dist_mat, 'ascend');
    k_index = order(1:num_k);
  else % Directly correspond the heads
    num_k = 3;
    head_model1 = models1_rot{1};
    YX1_head = [head_model1.y1, head_model1.x1];
    YX0 = zeros(num_rots, 2);
    for r = 1:num_rots
      model0_rot = models0_rot{r};
      head_model0 = model0_rot{1};
      YX0(r, :) = [head_model0.y1, head_model0.x1];
    end
    % Find top 3 fits on head position
    kd_tree_YX0 = vl_kdtreebuild(YX0');
    [k_index, ~] = vl_kdtreequery(kd_tree_YX0, YX0', YX1_head', 'NumNeighbors', num_k);
  end
  % Compare the torsos to find the best matching pose
  torso_model1 = models1_rot{center};
  torso_render1 = render_model({torso_model1}, z, 1);
  err_mat = zeros(1, num_k);
  for k = 1:num_k
    model0_rot = models0_rot{k_index(k)};
    torso_model0 = model0_rot{center};
    torso_render0 = render_model({torso_model0}, z, 1);
    err_mat(k) = numel(find((torso_render0-torso_render1) ~= 0));
    [k_index(k), deg_mat(k_index(k)), err_mat(k)]
  end
  orient_id = k_index(find(err_mat == min(err_mat)));
  orient_model = models0_rot{orient_id};
  % orient_render = render_model(orient_model, z, center);
  % figure; imagesc(orient_render)
  % Find the mapping between models0 and models1 parts
  id_mappings = containers.Map('KeyType', 'double', 'ValueType', 'any');
  % Find the part mapping between models0 and models1
  if ~exist('opt', 'var') || isempty(opt)
    YX0 = zeros(numel(orient_model), 2);
    for part_id = 1:numel(orient_model)
      part_model = orient_model{part_id};
      y0 = mean([part_model.y1]);
      x0 = mean([part_model.x1]);
      YX0(part_id, :) = [y0, x0];
      % [index, dist] = vl_kdtreequery(kd_tree_YX1, YX1', [y0, x0]', 'NumNeighbors', num_k);
      % % Get the top 5 parts it's closest to and see how well they fit
      % dist = sqrt(dist)*orient_model{center}.long_axis;
      % cost_mat = zeros(1, num_k);
      % for k = 1:num_k
      %   t_orient_model = orient_model;
      %   t_part_model = part_model;
      %   dy = YX1(index(k), 1)-y0;
      %   dx = YX1(index(k), 2)-x0;
      %   % Shift the ellipses to the appropriate centers
      %   for ell_id = 1:numel(part_model)
      %     t_part_model(ell_id).y1 = t_part_model(ell_id).y1+dy;
      %     t_part_model(ell_id).x1 = t_part_model(ell_id).x1+dx;
      %   end
      %   part_img = img;
      %   part_img(part_img ~= index(k)) = 0;
      %   part_img(part_img ~= 0) = 1;
      %   t_orient_model{part_id} = t_part_model;
      %   t_render = render_model(t_orient_model, z, center, [], part_id);
      %   t_render(t_render == 0.5) = 0;
      %   t_render(t_render > 0) = 1;
      %   % Cost is defined by dist+cov
      %   cost_mat(k) = numel(find(part_img-t_render ~= 0))+dist(k);
      %   figure; imagesc(part_img-t_render); title(strcat('cost=', num2str(cost_mat(k))))
      % end
      % min_cost_id = find(cost_mat == min(cost_mat));
      % if ~id_mappings.isKey(index(min_cost_id))
      %   id_mappings(index(min_cost_id)) = part_id;
      % else
      %   id_mappings(index(min_cost_id)) = [id_mappings(index(min_cost_id)), part_id];
      % end
    end
    num_parts = numel(orient_model);
    num_regions = numel(models1_rot);
    kd_tree_YX0 = vl_kdtreebuild(YX0');
    index_mat = zeros(num_regions, num_parts);
    dist_mat = zeros(num_regions, num_parts);
    for region_id = 1:num_regions
      y1 = YX1(region_id, 1);
      x1 = YX1(region_id, 2);
      [index, dist] = vl_kdtreequery(kd_tree_YX0, YX0', [y1, x1]', 'NumNeighbors', num_parts);
      [~, order] = sort(index, 'ascend');
      index_mat(region_id, :) = index(order);
      dist_mat(region_id, :) = dist(order);
    end
    dist_mat0 = dist_mat;
    used_regions = [];
    used_parts = [];
    attempts = zeros(1, num_parts);
    part_cnt = 0;
    while part_cnt < num_parts
      min_dist = min(dist_mat(:));
      [region_id, part_id] = find(dist_mat == min_dist);
      part_id = setdiff(part_id, used_parts);
      attempts(part_id) = attempts(part_id)+1;
      if attempts(part_id) < num_parts
        if ismember(region_id, used_regions)
          dist_mat(region_id, part_id) = inf;
          continue;
        end
      % elseif attempts(part_id) < num_parts
      %   % We need to compare the coverage of the part
      %   t_orient_model = orient_model;
      %   t_part_model = orient_model{part_id};
      %   y0 = mean([t_part_model.y1]);
      %   x0 = mean([t_part_model.x1]);
      %   dy = YX1(region_id, 1)-y0;
      %   dx = YX1(region_id, 2)-x0;
      %   % Shift the ellipses to the appropriate centers
      %   for ell_id = 1:numel(t_part_model)
      %     t_part_model(ell_id).y1 = t_part_model(ell_id).y1+dy;
      %     t_part_model(ell_id).x1 = t_part_model(ell_id).x1+dx;
      %   end
      %   part_img = img;
      %   part_img(part_img ~= region_id) = 0;
      %   part_img(part_img ~= 0) = 1;
      %   t_orient_model{part_id} = t_part_model;
      %   t_render = render_model(t_orient_model, z, center, [], part_id);
      %   t_render(t_render == 0.5) = 0;
      %   t_render(t_render > 0) = 1;
      %   % Cost is defined by coverage
      %   cost = numel(find(part_img-t_render == -1));
      %   figure; imagesc(part_img-t_render); title(strcat('cost=', num2str(cost)))
      %   % If we incur more cost than distance traveled then it is a worse tradeoff
      %   if cost > sqrt(min_dist)*t_orient_model{center}.long_axis;
      %     dist_mat(region_id, part_id) = inf;
      %     continue;
      %   end
      else % If nothing works, then just stick it back to the original spot
        part_id = part_id(1);
        region_dist = dist_mat0(:, part_id)
        region_id = find(region_dist == min(region_dist));
      end
      used_regions = [used_regions, region_id];
      used_parts = [used_parts, part_id];
      if ~id_mappings.isKey(region_id)
        id_mappings(region_id) = part_id;
      else
        id_mappings(region_id) = [id_mappings(region_id), part_id];
      end
      dist_mat(:, part_id) = inf;
      part_cnt = part_cnt+1;
    end

    id_mappings.keys
    id_mappings.values

    % Move each models0 ellipse to the closest region in models1

    for region_id = 1:num_regions
      if id_mappings.isKey(region_id)
        part_ids = id_mappings(region_id);
        % If 1 to 1 mapping then use their ellipse fit
        if numel(part_ids) == 1
          orient_model{part_ids} = models1_rot{region_id};
        else % Retain our original set of ellipses
          y0 = mean(YX0(part_ids, 1));
          x0 = mean(YX0(part_ids, 2));
          dy = YX1(region_id, 1)-y0;
          dx = YX1(region_id, 2)-x0;
          for p_id = 1:numel(part_ids)
            part_model = orient_model{part_ids(p_id)};
            for ell_id = 1:numel(part_model)
              part_model(ell_id).y1 = part_model(ell_id).y1+dy;
              part_model(ell_id).x1 = part_model(ell_id).x1+dx;
            end
            orient_model{part_ids(p_id)} = part_model;
          end
        end
      end
    end
  else % Use direct id mapping
    % For each models0 part, move them to correspond to models1 part
    for part_id = 1:numel(orient_model)
      % Each part_model0 corresponds to the same label part_model1
      if part_id > numel(models1_rot)
        orient_model(part_id) = [];
        break;
      end
      id_mappings(part_id) = part_id;
      part_model0 = orient_model{part_id};
      part_model1 = models1_rot{part_id};
      % Shift centroid of part_model0 to part_model1
      dy = mean([part_model1.y1])-mean([part_model0.y1]);
      dx = mean([part_model1.x1])-mean([part_model0.x1]);
      for ell_id = 1:numel(part_model0)
        part_model0(ell_id).y1 = part_model0(ell_id).y1+dy;
        part_model0(ell_id).x1 = part_model0(ell_id).x1+dx;
      end
      orient_model{part_id} = part_model0;
    end
  end
  pose_models.models = orient_model;
  pose_models.targets = models1_rot;
  pose_models.deg_rotate = deg_mat(orient_id);
  pose_models.deg_id = orient_id;
  orient_render = render_model(orient_model, z, center);
  figure; imagesc(orient_render)
end
