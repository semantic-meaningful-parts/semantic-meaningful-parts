% parts2patches
%
% Given a set of parts, extract the image patch from the
% corresponding image. Generate a set of weights for the 
% image patch based on the binary mask/segmentation.
%
% @param parts cell-array of symmetry axis pixels positions
% @param img intensity-matrix of image
% @param mask binary mask of image
% @return patches cell-array containing image patches of parts
% @return weights cell-array containing weights of patches

function [patches, weights, masks_pixels, centroids] = parts2patches(parts, img, mask, opt)
  params = get_params('parts2patches');
  if ~exist('opt', 'var') || isempty(opt)
    opt = 'edge';
  end
  % Number of parts to retrieve
  num_parts = numel(parts);
  [Y_MAX, X_MAX] = size(img);
  % Call helper to assign each pixel of mask to a part
  assigned_pixels = assign2parts(parts, mask, opt);
  % Extract the patches from the image
  patches = cell(1, num_parts);
  weights = cell(1, num_parts);
  centroids = cell(1, num_parts);
  masks_pixels = cell(1, num_parts);
  % For each part and its associated pixels
  for part_id = 1:num_parts
    part_pixels = assigned_pixels{part_id};
    symm_pixels = parts{part_id};
    part_pixels = [part_pixels; symm_pixels];
    % Get the bounding box for the pixels
    y_min = min(part_pixels(:, 1));
    y_min = max(1, y_min-params.BUFFER);
    x_min = min(part_pixels(:, 2));
    x_min = max(1, x_min-params.BUFFER);
    y_max = max(part_pixels(:, 1));
    y_max = min(Y_MAX, y_max+params.BUFFER);
    x_max = max(part_pixels(:, 2));
    x_max = min(X_MAX, x_max+params.BUFFER);
    % Extract the patch
    patch = img(y_min:y_max, x_min:x_max);
    % Create the set of weights based on the assigned pixels
    if strcmp(opt, 'full')
      % Compute centroid
      mask_pixels = part_pixels;
      part_mask = plot_pixels({mask_pixels}, mask);
      centroid = get_centroid(part_mask);
      % Get the offset of based on the min values
      yx_dim = [y_max-y_min+1, x_max-x_min+1];
      num_pixels = size(part_pixels, 1);
      offset = [y_min-1, x_min-1];
      part_pixels = part_pixels-repmat(offset, num_pixels, 1);
      % Get the subscripts as index and set them to 1 for weights.
      patch_weights = zeros(yx_dim);
      part_pixels_ind = sub2ind(yx_dim, part_pixels(:, 1), part_pixels(:, 2));
      patch_weights(part_pixels_ind) = 1;
      %SE = strel('square', 2);
      %patch_weights = imerode(patch_weights, SE);
      patch_weights = imfilter(double(patch_weights), params.G);
    elseif strcmp(opt, 'edge')
      SE = strel('square', 3);
      % Compute centroid
      part_mask = mask;
      part_mask(y_min:y_max, x_min:x_max) = part_mask(y_min:y_max, x_min:x_max)+1;
      part_mask(part_mask < 2) = 0;
      centroid = get_centroid(part_mask);
      % Get the pixels of this part
      [y_pixels, x_pixels] = find(part_mask > 0);
      mask_pixels = [y_pixels, x_pixels];
      % Extract patch weights
      patch_weights = mask(y_min:y_max, x_min:x_max);
      patch_weights = imerode(patch_weights, SE);
      % Weights follow a Gaussian distribution
      patch_weights = imfilter(double(patch_weights), params.G);
    end
    % Store the patch and patch weights
    patches{part_id} = patch;
    weights{part_id} = patch_weights;
    centroids{part_id} = centroid;
    masks_pixels{part_id} = mask_pixels;
    if params.VISUAL
      figure; 
      subplot(2, 2, 1); imagesc(patch);
      subplot(2, 2, 2); imagesc(patch_weights);
      subplot(2, 2, 3); imagesc(part_mask);
    end
  end % end-for-part_id
end % end-function
