% get_junctions
%
% @param parts cell array containing pixel location of parts 
% @return junctions a cell-array containing the junctions between parts
%
% Given a cell array of pixel location coordinates of parts
% we compute the set of junctions for each part connecting to
% another part.  

function junctions = get_junctions(parts, img)
  num_parts = numel(parts);  
  junctions = cell(1, num_parts);
  for p_idx = 1:num_parts
    % Map of pixel location of junctions to part id
    connections = containers.Map('KeyType', 'double', 'ValueType', 'any');
    p_part = parts{p_idx};
    [y_dim, x_dim] = size(img);
    for q_idx = 1:num_parts
      % Skip if the same part
      if p_idx == q_idx
        continue;
      end
      % See if ppart and qpart are connected
      q_part = parts{q_idx};
      conn = intersect(p_part, q_part, 'rows');
      num_conn = size(conn, 1);
      for conn_idx = 1:num_conn
        conn_key = conn(conn_idx, :);
        junction_key = yx2index([1, 1; y_dim, x_dim], conn_key);
        % If the key already exists (ie. a complex junction)
        if connections.isKey(junction_key)
          % Append the value to the current key value
          junction_value = connections(junction_key);
          junction_value = [junction_value; q_idx];
          connections(junction_key) = junction_value;
        else
          % Simply store the part id (q_idx) as the value
          connections(junction_key) = q_idx;
        end % end-if-connections.isKey(junction_key)
      end
    end
    % Store the junctions
    junctions{p_idx} = connections;
  end
end
