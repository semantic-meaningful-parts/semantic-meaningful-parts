% meaningful_patches
%
% @param dataset dataset containing images
% @param sample_id sample index within the dataset
% @param class_id class label for the particular sample
% @return patch_info struct containing the meaningful parts extracted
% from the image
%
% Given an image from a dataset, apply symmetry axis transform and 
% decompose the axis into a set of parts by junctions. Extract the image
% patches corresponding to the parts. We denote these patches as 
% meaningful patches.

function patch_struct = meaningful_parts(img, mask)
  % Set up internal parameters
  params = get_params('meaningful_parts');
  % If we are not given the mask
  if ~exist('mask', 'var') || isempty(mask)
    mask = img;
    % Set up image to prune out weaker pixels
    mask_arr = reshape(mask, 1, []);
    std_dev = std(double(mask_arr));
    max_value = max(mask_arr);
    strength_lim = max_value-(params.OUTLIER_MULT*std_dev);
    % Remove weak pixels
    mask(mask < strength_lim) = 0;
    % Generate our own mask
    mask(mask > 0) = 1;
  end % end-if-!exist
  % Total time start
  time_start = tic;
  % Get symmetry axis
  sa_start = tic;
  skeleton = symmetry_axis(mask, 'default');
  sa_elapsed = toc(sa_start);
  % Decompose the symmetry axis to segments, this is our initial grouping
  d2s_start = tic;
  segments = decompose2segments(skeleton);
  d2s_elasped = toc(d2s_start);
  % Takes the segments and joins them into parts
  c2p_start = tic;
  parts = compose2parts(segments, mask);
  c2p_elasped = toc(c2p_start);
  % Extract the patches and weights corresponding to the parts
  p2p_start = tic;
  [patches, patch_weights, masks_pixels, centroids] = parts2patches(parts, ...
                                                                    img, ...
                                                                    mask, ...
                                                                    params.PARTS2PATCHES_OPT);
  p2p_elapsed = toc(p2p_start);
  % Generate connectivity structure for each part and its junctions 
  gj_start = tic;
  junctions = get_junctions(parts, img);
  gj_elasped = toc(gj_start);

  %lp_start = tic;
  %label_parts(parts, junctions, mask);
  %lp_elasped = toc(lp_start);

  % Get relative parts centered at [0,0]
  grp_start = tic;
  relative_parts = batch_get_relative_part(parts);
  grp_elasped = toc(grp_start);
  % Get relative junctions centered at [0,0]
  grj_start = tic;
  relative_junctions = batch_get_relative_junctions(junctions, parts, size(mask));
  grj_elasped = toc(grp_start);
  % Populate the meaningful patch structure with data
  patch_struct = populate_patch_struct(parts, ...
                                       patches, ...
                                       patch_weights, ...
                                       junctions, ...
                                       relative_parts, ...
                                       relative_junctions, ...
                                       masks_pixels, ...
                                       centroids);
  % Total time elapsed
  time_elasped = toc(time_start);

  if params.VISUAL
    semantic_mask = zeros(size(mask));
    num_parts = numel(parts);
    % Generate the semantic mask
    for part_id = 1:num_parts
      mask_pixels = masks_pixels{part_id};
      mask_pixels_ind = sub2ind(size(mask), mask_pixels(:, 1), mask_pixels(:, 2));
      semantic_mask(mask_pixels_ind) = part_id;
    end
    num_patches = numel(patches)*2;
    num_figures = num_patches+6;
    num_cols = 4;
    num_rows = ceil(num_figures/num_cols);
    figure;
    % Plot important figures of step progression
    subplot(num_rows, num_cols, 1); imagesc(img);
    subplot(num_rows, num_cols, 2); imagesc(mask);
    subplot(num_rows, num_cols, 3); imagesc(skeleton);
    subplot(num_rows, num_cols, 4); imagesc(plot_pixels(segments, mask, 'overlay'));
    subplot(num_rows, num_cols, 5); imagesc(plot_pixels(parts, mask, 'overlay'));
    subplot(num_rows, num_cols, 6); imagesc(semantic_mask);
    % Add centroid markers to semantic mask
    for part_id = 1:num_parts
      hold on;
      yx = centroids{part_id};
      plot(yx(params.x), yx(params.y), 'ko', 'MarkerSize', 5);
      hold off;
    end
    % Plot patches and patch weights
    img_idx = 1;
    for fig_idx = 7:num_figures
      if mod(fig_idx, 2) == 1
        subplot(num_rows, num_cols, fig_idx); imagesc(patches{img_idx});
      else
        subplot(num_rows, num_cols, fig_idx); imagesc(patch_weights{img_idx});
        img_idx = img_idx+1;
      end
    end
  end

  if params.VOCAL
    fprintf('Elasped time for extracting symmetry axis: %.3f seconds\n', ...
      sa_elapsed);
    fprintf('Elasped time for decomposing to segments: %.3f seconds\n', ...
      d2s_elasped);
    fprintf('Elasped time for composing segments to parts: %.3f seconds\n', ...
      c2p_elasped);
    fprintf('Elasped time for extracting patches and weights: %.3f seconds\n', ...
      p2p_elasped);
    fprintf('Elasped time for getting the junctions: %.3f seconds\n', ...
      gj_elasped);
    fprintf('Elasped time for generating relative parts: %.3f seconds\n', ...
      grp_elasped);
    fprintf('Elasped time for generating relative junctions: %.3f seconds\n', ...
      grj_elasped);

    fprintf('Total time incurred: %.3f seconds\n', ...
      time_elasped); 
  end
end % end-function

% Helper function
% Run get_relative_part in batch
function relative_parts = batch_get_relative_part(parts)
  num_parts = numel(parts);
  relative_parts = cell(1, num_parts);
  for part_id = 1:num_parts
    part = parts{part_id};
    relative_part = get_relative_part(part);
    relative_parts{part_id} = relative_part;
  end % end-for-part_id
end % end-function

% Helper function
% Run get_relative_junctions in batch
function relative_junctions = batch_get_relative_junctions(junctions, parts, yx_dim)
  num_parts = numel(parts);
  relative_junctions = cell(1, num_parts);
  for part_id = 1:num_parts
    part = parts{part_id};
    connections = junctions{part_id};
    relative_connections = get_relative_junctions(connections, part, yx_dim);
    relative_junctions{part_id} = relative_connections;
  end % end-for-part_id
end % end-function

% Helper function
% Populates meaningful patch structure with data
function patch_struct = populate_patch_struct(parts, ...
                                              patches, ...
                                              patch_weights, ...
                                              junctions, ...
                                              relative_parts, ...
                                              relative_junctions, ...
                                              masks_pixels, ...
                                              centroids)
  % Set up internal parameters
  params = get_params('meaningful_parts');
  patch_struct = params.PATCH_STRUCT;
  num_parts = numel(parts);
  for part_id = 1:num_parts
    patch_struct(part_id).part = parts{part_id};
    patch_struct(part_id).patch = patches{part_id};
    patch_struct(part_id).patch_weights = patch_weights{part_id};
    patch_struct(part_id).junctions = junctions{part_id};
    patch_struct(part_id).relative_part = relative_parts{part_id};
    patch_struct(part_id).relative_junctions = relative_junctions{part_id};
    patch_struct(part_id).part_mask = masks_pixels{part_id};
    patch_struct(part_id).centroid = centroids{part_id};
  end % end-for-part_id
end % end-function
