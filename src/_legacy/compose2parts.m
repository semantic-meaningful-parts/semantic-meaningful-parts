% compose2parts
%
% @param segments symmetry axis segments in pixel space stored as a cell array
% @return parts symmetry axis segments composed via inner angle stored as a cell array
% 
% Takes the segments of a symmetry axis and recompose the segments into parts
% segments based on inner angle junctions.

function parts = compose2parts(segments, mask)
  params = get_params('compose2parts');
  % Constants
  MIN_AFFINITY = params.CONTOUR_AFFINITY_THRESH+params.SYMM_AXIS_AFFINITY_THRESH;
  TERMINAL_AFFINITY = MIN_AFFINITY*0.90;
  num_segments = numel(segments);
  is_merged = false(1, num_segments);
  % First find the set of joint complexities
  complexities = joint_complexity(segments);
  % Sort the branches such that we process terminals first
  terminal_set = is_terminal(complexities);
  terminal_ids = find(terminal_set == true);
  general_ids = setdiff(1:num_segments, terminal_ids);
  % Parition our segments and complexities into terminal and genearl set
  terminal_segments = segments(terminal_ids);
  general_segments = segments(general_ids);
  terminal_complexities = complexities(:, terminal_ids);
  general_complexities = complexities(:, general_ids);
  % Sort by merging the sets such that we process terminals first
  segments = [terminal_segments, general_segments];
  complexities = [terminal_complexities, general_complexities];
  terminal_ids = is_terminal(complexities);
  % Assign edge contour pixels to each segment
  contours = assign2parts(segments, mask, 'edge');
  % Generate contours associated with each segment
  contour_maps = cell(1, num_segments);
  no_contours = false(1, num_segments);
  for contour_id = 1:num_segments
    contour = contours{contour_id};
    if isempty(contour)
      % Find the branches that are not mapped to any edges
      no_contours(contour_id) = true;
    end
    contour_maps{contour_id} = plot_pixels({contour}, mask);
  end % end-contour_id
  % Merge segments with no associated contours first
  no_contour_ids = find(no_contours == true);
  [segments, no_contour_merged] = compose_no_contours(no_contour_ids, ...
                                                      segments, ...
                                                      terminal_ids, ...
                                                      params.SYMM_AXIS_SAMPLE_SIZE);
  is_merged(no_contour_merged) = true;
  % Merge general segments when given large inner angle
  for curr_id = 1:num_segments
    if is_merged(curr_id)
      continue;
    end
    curr_segment = segments{curr_id};
    num_curr_pixels = size(curr_segment, 1);
    sample_size_A = min(num_curr_pixels-1, params.SYMM_AXIS_SAMPLE_SIZE);
    % Get the first and last pixels of segment
    curr_front = curr_segment(1, :);
    curr_end = curr_segment(end, :);
    % Store the angle/affinity
    symm_axis_affinity_mat = zeros(1, num_segments);
    contour_affinity_mat = zeros(1, num_segments);
    % Iterate through all of the segments
    for next_id = curr_id+1:num_segments
      if is_merged(next_id)
        continue;
      end
      % Get the next segment we will look at 
      next_segment = segments{next_id};
      num_next_pixels = size(next_segment, 1);
      % Ideally remove this condition
      if num_curr_pixels > params.MAX_SIZE_FOR_MERGE && ...
         num_next_pixels > params.MAX_SIZE_FOR_MERGE
        continue;
      end
      sample_size_B = min(num_next_pixels-1, params.SYMM_AXIS_SAMPLE_SIZE);
      % Get the first and last pixels of segment
      next_front = next_segment(1, :);
      next_end = next_segment(end, :);
      % Check if the two are the share a front or end
      connected = false;
      sample_A = []; sample_B = [];
      % Check which ends may be connected
      if pdist([curr_front; next_front]) < 3
        % We want ends to meet
        sample_A = flipud(curr_segment);
        sample_B = flipud(next_segment);
        connected = true;
      elseif pdist([curr_front; next_end]) < 3
        sample_A = flipud(curr_segment);
        sample_B = next_segment;
        connected = true;
      elseif pdist([curr_end; next_front]) < 3
        sample_A = curr_segment;
        sample_B = flipud(next_segment);
        connected = true;
      elseif pdist([curr_end; next_end]) < 3
        sample_A = curr_segment;
        sample_B = next_segment;
        connected = true;
      end % end-if-isequal(curr_front, next_front)
      % If they are connected, see if they should be merged
      if connected
        % Take a sample
        sample_A = sample_A(end-sample_size_A:end, :);
        sample_B = sample_B(end-sample_size_B:end, :);
        % Measure the inner angle of the samples
        theta = inner_angle(sample_A, sample_B);
        symm_axis_affinity_mat(next_id) = theta;
        % Add another measurement based on edge contour
        contour_A = contours{curr_id}; map_A = contour_maps{curr_id};
        contour_B = contours{next_id}; map_B = contour_maps{next_id};
        rho = NaN;
        % Make sure that there is an associated contour
        if size(contour_A, 1) > 0 && size(contour_B, 1) > 0
          CONTOUR_SAMPLE_SIZE = min(num_curr_pixels, num_next_pixels);
          rho = contour_affinity(contour_A, map_A, contour_B, map_B, CONTOUR_SAMPLE_SIZE);
          contour_affinity_mat(next_id) = rho;
        end % end-if-size(contour...)
        % If there isn't then it is simply NaN, should be merged
        if isnan(rho)
          contour_affinity_mat(next_id) =  theta;
        end
      end % end-if-connected
    end % end-for-next_id
    % Get the scores of the affinities based on symmetry axis and contour
    affinity_mat = symm_axis_affinity_mat+contour_affinity_mat;
    % Check complexity of the current segment
    curr_cmplx = complexities(:, curr_id);
    curr_endpoint = find(curr_cmplx == 0);
    % If this is a terminal branch/segment
    if ~isempty(curr_endpoint)
      % Get the ids of the connect components
      connected_ids = find(symm_axis_affinity_mat > 0);
      num_connected_ids = numel(connected_ids);
      for idx = 1:num_connected_ids
        connected_id = connected_ids(idx);
        conn_cmplx = complexities(:, connected_id);
        conn_endpoint = find(conn_cmplx == 0);
        conn_segment = segments{connected_id};
        num_conn_pixels = size(conn_segment, 1);
        conn_affinity = affinity_mat(connected_id);
        if ~isempty(conn_endpoint) && ...
           (conn_affinity > TERMINAL_AFFINITY || ...
           num_conn_pixels < params.MIN_SIZE_ALLOWED || num_curr_pixels < params.MIN_SIZE_ALLOWED)
          merged_segment = merge_segments(segments{curr_id}, segments{connected_id}, 3, 'inverse');
          [merged_contour, merged_map] = merge_contours(contours{curr_id}, ...
                                                        contour_maps{curr_id}, ...
                                                        contours{connected_id}, ...
                                                        contour_maps{connected_id});
          segments{curr_id} = merged_segment;
          contours{curr_id} = merged_contour;
          contour_maps{curr_id} = merged_map;
          is_merged(connected_id) = true;
        end % end-if-~isempty(next_endpoint)
      end % end-for-idx
    end % end-if-~isempty(curr_endpoint)
    % Get the largest affinity between connected segments
    merged_id = find(is_merged == true);
    affinity_mat(merged_id) = 0;
    max_affinity = max(affinity_mat);
    % Only interested in affinity that actually is above threshold
    if max_affinity > MIN_AFFINITY
      max_id = find(affinity_mat == max_affinity);
      if ~is_merged(max_id)
        % Merge the current segment and its contours with that with max_id
        merged_segment = merge_segments(segments{curr_id}, segments{max_id}, 3, 'default');
        [merged_contour, merged_map] = merge_contours(contours{curr_id}, ...
                                                      contour_maps{curr_id}, ...
                                                      contours{max_id}, ...
                                                      contour_maps{max_id});
        segments{max_id} = merged_segment;
        contours{max_id} = merged_contour;
        contour_maps{max_id} = merged_map;
        is_merged(curr_id) = true;
      end % end-if-~is_merged(max_id)
    end % end-if-(max_theta > 0)
  end % end-for-curr_id
  merged_ids = find(is_merged == true);
  retained = setdiff(1:num_segments, merged_ids);
  parts = segments(retained);
end % end-function




% Helper function
% Computes the inner angle between the contours of A and B
function rho = contour_affinity(contour_A, map_A, contour_B, map_B, SAMPLE_SIZE)
  params = get_params('default');
  Y_MIN = 1; X_MIN = 1; Y_MAX = size(map_A, 1); X_MAX = size(map_A, 2);
  % Part contours are connected when they share at least 1 symmetry axis pixel
  shared_contour = intersect(contour_A, contour_B, 'rows');
  num_contour_A = size(contour_A, 1);
  num_shared = size(shared_contour, 1);
  % If they don't share the same pixel, then look at neighborhood of A
  if num_shared == 0
    % Look at the pixels in contour_A
    for point_id = 1:num_contour_A
      point_A = contour_A(point_id, :);
      neighborhood_A = map_B(max(point_A(params.y)-1, Y_MIN):min(point_A(params.y)+1, Y_MAX), ...
                             max(point_A(params.x)-1, X_MIN):min(point_A(params.x)+1, X_MAX));
      shared = find(neighborhood_A == 1);
      if numel(shared) > 0
        num_shared = num_shared+1;
        shared_contour(num_shared, :) = [point_A(params.y), point_A(params.x)];
      end
    end % end-for-point_id
  end % end-if-size(shared_contour, 1) == 0
  rho_mat = zeros(1, num_shared);
  for shared_id = 1:num_shared
    shared_pixel = shared_contour(shared_id, :);
    point_A = shared_pixel;
    point_B = shared_pixel;
    map_A(point_A(params.y), point_A(params.x)) = ...
        map_A(point_A(params.y), point_A(params.x))+2;
    map_B(point_B(params.y), point_B(params.x)) = ...
        map_B(point_B(params.y), point_B(params.x))+2;
    continue_A = true; continue_B = true;
    for sample_id = 1:SAMPLE_SIZE
      % Follow the singly connected path of contour map of A from the shared pixel
      if continue_A
        % Look around the current pixel for the next pixel
        neighborhood_A = map_A(point_A(params.y)-1:point_A(params.y)+1, ...
                               point_A(params.x)-1:point_A(params.x)+1);
        [y_A, x_A] = find(neighborhood_A == 1); 
        if size(y_A, 1) > 0
          point_A = [point_A(params.y)+y_A(params.first)-2, ...
                     point_A(params.x)+x_A(params.first)-2];
          map_A(point_A(params.y), point_A(params.x)) = ...
              map_A(point_A(params.y), point_A(params.x))+2; 
        else
          continue_A = false;
        end
      end % end-if-continue_A
      % Do the same for part B
      if continue_B
        % Look around the current pixel for the next pixel
        neighborhood_B = map_B(point_B(params.y)-1:point_B(params.y)+1, ...
                               point_B(params.x)-1:point_B(params.x)+1);
        [y_B, x_B] = find(neighborhood_B == 1);
        if size(y_B, 1) > 0
          point_B = [point_B(params.y)+y_B(params.first)-2, ...
                     point_B(params.x)+x_B(params.first)-2];
          map_B(point_B(params.y), point_B(params.x)) = ...
              map_B(point_B(params.y), point_B(params.x))+2; 
        else
          continue_B = false; 
        end
      end % end-if-continue_B
    end % end-for-sample_id
    % Reset the contour maps
    map_A(map_A > 0) = 1; map_B(map_B > 0) = 1;
    % Form the vectors A and B
    vector_A = [point_A; shared_pixel];
    vector_B = [point_B; shared_pixel];
    % Compute the angles between A and B
    rho_mat(shared_id) = inner_angle(vector_A, vector_B);
  end
  % In case the two segments don't connect on contour
  if isempty(rho_mat)
    rho = NaN;
  else
    rho = max(rho_mat);
  end
end % end-function

function [segments, merged_ids] = compose_no_contours(no_contour_ids, segments, invalid, SAMPLE_SIZE)
  num_segments = numel(segments);
  num_ids = numel(no_contour_ids);
  is_merged = false(1, num_segments);
  for no_contour_id = 1:num_ids
    curr_id = no_contour_ids(no_contour_id);
    if is_merged(curr_id) || ismember(curr_id, invalid)
      continue;
    end
    % Define a set of scores for segment affinity
    symm_axis_affinity_mat = zeros(1, num_segments);
    % Get the current segment
    curr_segment = segments{curr_id};
    curr_front = curr_segment(1, :);
    curr_end = curr_segment(end, :);
    % Find the appropriate size to extract for sample
    num_curr_pixels = size(curr_segment, 1);
    sample_size_A = min(num_curr_pixels-1, SAMPLE_SIZE);
    for next_id = 1:num_segments
      % Sanity check
      if curr_id == next_id || is_merged(next_id)
        continue;
      end
      next_segment = segments{next_id};
      next_front = next_segment(1, :);
      next_end = next_segment(end, :);
      % Find the appropriate size to extract for sample
      num_next_pixels = size(next_segment, 1);
      sample_size_B = min(num_next_pixels-1, SAMPLE_SIZE);
      % Check which ends may be connected
      connected = false; sample_A = []; sample_B = [];
      if pdist([curr_front; next_front]) < 3
        % We want ends to meet
        sample_A = flipud(curr_segment);
        sample_B = flipud(next_segment);
        connected = true;
      elseif pdist([curr_front; next_end]) < 3
        sample_A = flipud(curr_segment);
        sample_B = next_segment;
        connected = true;
      elseif pdist([curr_end; next_front]) < 3
        sample_A = curr_segment;
        sample_B = flipud(next_segment);
        connected = true;
      elseif pdist([curr_end; next_end]) < 3
        sample_A = curr_segment;
        sample_B = next_segment;
        connected = true;
      end % end-if-isequal(curr_front, next_front)
      % If they are connected, see if they should be merged
      if connected
        % Take a sample
        sample_A = sample_A(end-sample_size_A:end, :);
        sample_B = sample_B(end-sample_size_B:end, :);
        % Measure the inner angle of the samples
        theta = inner_angle(sample_A, sample_B);
        symm_axis_affinity_mat(next_id) = theta;
      end
    end % end-for-segment_id
    % Find the best one to merge
    symm_axis_affinity_mat(invalid) = 0;
    max_affinity = max(symm_axis_affinity_mat);
    max_id = find(symm_axis_affinity_mat == max_affinity);
    if ~is_merged(max_id)
      % Merge the current segment and its contours with that with max_id
      merged_segment = merge_segments(segments{curr_id}, segments{max_id}, 3, 'default');
      segments{max_id} = merged_segment;
      is_merged(curr_id) = true;
    end % end-if-~is_merged(max_id)
  end % end-for-no_contour_id
  merged_ids = find(is_merged == true);
end

% Helper function
% Merges the pixel contours and the contour maps of A and B
function [contour, map] = merge_contours(contour_A, map_A, contour_B, map_B)
  map = map_A+map_B;
  map(map > 1) = 1;
  contour = [contour_A; contour_B];
  contour = unique(contour, 'rows');
end % end-function
