
% Helper function
% Merges segment A and B at some pixel by flipping them accordingly 
function merged_segment = merge_segments(segment_A, segment_B, NEIGHBOR_RADIUS, opt)
  if ~exist('opt', 'var') || isempty(opt)
    opt = 'default';
  end
  merged_segment = [];
  % Get the first and last pixels of segments
  first_A = segment_A(1, :);
  last_A = segment_A(end, :);
  first_B = segment_B(1, :);
  last_B = segment_B(end, :);
  if strcmp(opt, 'default')
    % Check if the segments are connected
    if pdist([first_A; first_B]) < NEIGHBOR_RADIUS
      % Now end of A meets front of B
      segment_A = flipud(segment_A);
      segment_B = segment_B(2:end, :);
      % End of A now meets front of B
      merged_segment = [segment_A; segment_B];
    elseif pdist([first_A; last_B]) < NEIGHBOR_RADIUS
      % Simply stack them together
      segment_A = segment_A(2:end, :);
      merged_segment = [segment_B; segment_A];
    elseif pdist([last_A; first_B]) < NEIGHBOR_RADIUS
      % Simply stack them together
      segment_B = segment_B(2:end, :);
      merged_segment = [segment_A; segment_B];
    elseif pdist([last_A; last_B]) < NEIGHBOR_RADIUS
      % Now end of A meets front of B
      segment_B = flipud(segment_B);
      segment_B = segment_B(2:end, :);
      merged_segment = [segment_A; segment_B];
    end % end-if-isequal
  elseif strcmp(opt, 'inverse')
% Check if the segments are connected
    if pdist([first_A; first_B]) < NEIGHBOR_RADIUS
      % Simply stack them so we preserve the connecting point
      segment_B = segment_B(1:end-1, :);
      merged_segment = [segment_A; segment_B];
    elseif pdist([first_A; last_B]) < NEIGHBOR_RADIUS
      % Reverse B so we preserve the connecting point
      segment_A = segment_A(2:end, :);
      segment_B = flipud(segment_B);
      merged_segment = [segment_B; segment_A];
    elseif pdist([last_A; first_B]) < NEIGHBOR_RADIUS
      % Reverse A so we preserve the connecting point
      segment_A = flipud(segment_A);
      segment_B = segment_B(2:end, :);
      merged_segment = [segment_A; segment_B];
    elseif pdist([last_A; last_B]) < NEIGHBOR_RADIUS
      % Preserve last_B so that we preserve the connecting point
      segment_A = segment_A(1:end-1, :);
      merged_segment = [segment_A; segment_B];
    end % end-if-isequal
  end % end-if-strcmp(opt, ...)
end % end-function



