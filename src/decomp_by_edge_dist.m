function osegments = decomp_by_edge_dist(segments, mask)

% % to see all the segments
% fig; imagesc(plot_pixels(segments, mask, 'overlay'))

% example 3: frontal horse
% example 2: 

nsegments = length(segments);
osegments = {};
for k = 1:nsegments;
  osegments = cat(2, osegments, split_segment(segments{k}, mask, segments, k));
end
end

%-----------------------------------------------------------------------------
% fxn to split segments
%-----------------------------------------------------------------------------
function osegs = split_segment(seg, mask, segs, segid);

[rows,cols] = size(mask);
osegs = seg;
MAXPXLLOOK = 3;
N = size(seg, 1);
% SPLITTHRESH = 0.1; % 10% of length total change --> new split
% SPLITTHRESH = 2; % 10% of length total change --> new split
SPLITTHRESH = max(2, log(N)); % 10% of length total change --> new split
SPLITLENGTH = max(5, floor(0.1*N)); % min # of pixels for change to be true
SPLITSIZE = 2*SPLITLENGTH+1;

if N < SPLITSIZE; return; end; % if too small already, don't check for splits


% 1. get angle for each pxl in seg
d = zeros(N,2); % y and x directions
for k = 1:N;
  dp = seg(min(N,k+1:1:k+MAXPXLLOOK),:) - seg(max(1,k-1:-1:k-MAXPXLLOOK),:);
  d(k,:) = mean(dp);
end
d = bsxfun(@rdivide, d, sqrt(sum(d.^2,2))); % normalizes direction
	
% 2. for each pixel, walk at orthogonal angle to compute strokewidth
sl = zeros(N,2); % strokevectors
for k = 1:N;
  d1 = [d(k,2),-d(k,1)];

  % look for edge/border/what have you
  p = seg(k,:); kp = 0; m1 = 1; m2 = 1;
  while((m1>0) && (m2>0) && (kp<500)); % random large threshold
    kp = kp+1;
    p1 = max(1, round(p + kp.*d1));
    p1(1) = min(p1(1),rows); p1(2) = min(p1(2),cols);
    m1 = mask(p1(1),p1(2));
    p2 = max(1, round(p - kp.*d1)); 
    p2(1) = min(p2(1),rows); p2(2) = min(p2(2),cols);
    m2 = mask(p2(1),p2(2));
  end
  sl(k,:) = p2 - p1;
end
sl = sqrt(sum(sl.^2,2)); % strokelengths

% 3. make splits happen
% 3.1 detecting splits with split filter
spl = zeros(N,1);
cmin = sl(1);
for k = SPLITLENGTH+1:(N-SPLITLENGTH);
  spl(k) = filter_kinks(sl(k-SPLITLENGTH:k+SPLITLENGTH));
end

% 3.2 finding local max of filter responses
breakpoints = [];
k0 = 1; k1 = 1;
k = SPLITLENGTH;
while k < N-SPLITLENGTH;
% for k = SPLITLENGTH:(N-SPLITLENGTH);
  k0 = k; 
  if spl(k) > SPLITTHRESH;
    while spl(k) > 0;
      k = k+1;
    end
    % [spl_max, imax] = max(spl(k0:k-1));
    % breakpoints = cat(1, breakpoints, k0+imax-1);
    [spl_max, imax] = max(spl((k0-SPLITLENGTH):(k-1+SPLITLENGTH)));
    breakpoints = cat(1, breakpoints, k0-SPLITLENGTH+imax-1);
  end
  % fprintf('%d \n', k);
  k = k+1;
end

% 3.3 divide the segment into the splitted up regions
breakpoints = unique(breakpoints);
breakpoints = cat(1, 1, breakpoints, N);
splbreak = spl(breakpoints);
kb = 1;
% display(segid); if segid == 3; keyboard; end;
while kb < length(breakpoints);
  bl = breakpoints(kb);
  br = breakpoints(kb+1);
  l = br-bl+1;
  if l < SPLITSIZE;
    spl_l = splbreak(kb);
    spl_r = splbreak(kb+1);
    if kb == 1; spl_r = -100; end;
    if (kb+1) == length(breakpoints); spl_l = -100; end;

    % quick error check
    if (spl_r == -100 && spl_l == -100); 
      fprintf(['%s: ERROR: segment is too small, should have never split' ...
        'at all! (l.29 allowing splitting tiny segments\n'], mfilename); 
      keyboard;
    end

    if spl_l < spl_r; % merge left
      splbreak(kb) = [];
      breakpoints(kb) = [];
      kb = max(1, kb-1);
    else % merge right
      splbreak(kb+1) = [];
      breakpoints(kb+1) = [];
    end
  else
    kb = kb+1;
  end
end

Nsegs = length(breakpoints)-1;
osegs = {};
for kb = 1:Nsegs;
  osegs{kb} = seg(breakpoints(kb):breakpoints(kb+1),:);
end

% keyboard;
end

% detection of kink mechanism
function d = filter_kinks(sl);
N = length(sl);
% d = min(sl(1) - sl(round(N/2)), sl(N) - sl(round(N/2)));
d = 0.5*(sl(1) - sl(round(N/2))) + 0.5*(sl(N) - sl(round(N/2)));
end
