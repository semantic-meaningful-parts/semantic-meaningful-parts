% Update masks to most recent fixes
apply_horse_cow_changes
% Set up internal parameters
VISUAL = false;
% Necessary data for segmentation
imgs_original = horse_training_images;
imgs_seg = horse_training_images_seg;
imgs_bw_seg = horse_training_images_bw_seg;
img_masks = horse_training_masks;
% Ground truth labels for data
gnd_truth = horse_training_labels;
num_testing = numel(imgs_original);
mean_iou_scores = zeros(1, num_testing);
% Test with intersection over union (IOU)
for test_num = 1:10
  % Get our meaningful parts
  img_seg = imgs_seg{test_num};
  img_bw_seg = imgs_bw_seg{test_num};
  mask = img_masks{test_num};
  part_struct = meaningful_parts(img_bw_seg, mask);
  num_parts = numel(part_struct);
  % Generate the semantic full mask and part masks
  semantic_mask = zeros(size(mask));
  semantic_parts = cell(1, num_parts);
  for part_id = 1:num_parts
    part_mask = part_struct(part_id).part_mask;
    % Generate the semantic mask
    mask_pixels_ind = sub2ind(size(mask), part_mask(:, 1), part_mask(:, 2));
    semantic_mask(mask_pixels_ind) = part_id;
    % Generate the semantic parts individually
    semantic_part = zeros(size(mask));
    semantic_part(mask_pixels_ind) = 1;
    semantic_parts{part_id} = semantic_part;
  end % end-for-part_id
  % Get the ground truth labeling
  gnd_truth_labels_img = gnd_truth{test_num};
  num_labels = max(gnd_truth_labels_img(:));
  gnd_truth_labels = cell(1, num_labels);
  % Iterate through each label to find the best match
  for label_id = 1:num_labels
    gnd_truth_label = gnd_truth_labels_img;
    % Generate the part labels
    gnd_truth_label(gnd_truth_label ~= label_id) = 0;
    gnd_truth_label(gnd_truth_label > 0) = 1;
    gnd_truth_labels{label_id} = gnd_truth_label;
  end % end-for-label_id
  % Perform IOU
  selected_masks = cell(1, num_labels);
  iou_scores = zeros(1, num_labels);
  for label_id = 1:num_labels
    gnd_truth_label = gnd_truth_labels{label_id};
    % Iterate through all of the retrieved parts and find the top IOU
    part_scores = zeros(1, num_parts);
    for part_id = 1:num_parts
      part_mask = semantic_parts{part_id};
      sum_mask = double(part_mask)+double(gnd_truth_label);
      intrsct_mask = sum_mask;
      intrsct_mask(intrsct_mask ~= 2) = 0;
      intrsct_mask(intrsct_mask == 2) = 1;
      union_mask = sum_mask;
      union_mask(union_mask ~= 0) = 1;
      iou = double(sum(intrsct_mask(:)))/sum(union_mask(:));
      part_scores(part_id) = iou;
    end
    max_iou = max(part_scores);
    max_id = find(part_scores == max_iou);
    selected_masks{label_id} = semantic_parts{max_id};
    iou_scores(label_id) = max_iou;
  end
  % Get mean IOU for the given image
  mean_iou_scores(test_num) = mean(iou_scores);
  % Visualize the retrieved parts
  if VISUAL
    figure_arr = [gnd_truth_labels; selected_masks];
    figure_arr = [{img_seg}; {gnd_truth_labels_img}; {semantic_mask}; figure_arr(:)];
    num_figures = numel(figure_arr);
    num_cols = 3;
    num_rows = double(ceil(num_figures/num_cols));
    figure;
    label_id = 1;
    for fig_id = 1:num_figures
      if fig_id == 3
        title(['mean iou score=', num2str(mean_iou_scores(test_num))]);
      elseif fig_id > 3 && mod(fig_id, 2) == 1
        title(['iou score=', num2str(iou_scores(label_id))]);
        label_id = label_id+1;
      end
      subplot(num_rows, num_cols, double(fig_id)); imagesc(figure_arr{fig_id});
    end
    suptitle(['Visualization for Test Image #', num2str(test_num)]);
  end % end-if-VISUAL
end % end-for-test_num



