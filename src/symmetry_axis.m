% symmetry_axis
%
% @param bin_img N by M matrix segmented object (white on black)
% @return skeleton symmetry axis of the segmented object
% 
% Takes an image and applies symmetry axis transform to acquire a
% skeleton of the object in the image.

function symm_axis = symmetry_axis(bin_img, opt)
  if ~exist('opt', 'var') || isempty(opt)
    opt = 'default';
  end
    % Set up internal parameters
  params = get_params('symmetry_axis');
  % Continuous thinning until single pixel
  if strcmp(opt, 'default')
    symm_axis = bwmorph(bin_img, 'thin', inf);
  elseif strcmp(opt, 'telea')
    RADIUS_THRESHOLD = 75;
    [skr, rad] = skeleton_telea(bin_img);
    symm_axis = bwmorph(skr > RADIUS_THRESHOLD, 'thin', inf);
  end
end