function [R, mean_iou] = run_experiments(train_imgs, test_imgs, test_masks, gnd_truth)
  % Quick test option
  if nargin == 0 % If no arguments were given
    setup_data;
    train_imgs = horse_training_labels(1);
    test_imgs = horse_training_images_seg(1:2);
    test_masks = horse_training_masks(1:2);
    gnd_truth = horse_training_labels(1:2);
  end % end-if-no-args
  % Set up training images - labels for parts
  model = train_model(train_imgs);
  % Use model to generate part labeling for each test images
  num_test = numel(test_imgs);
  mean_iou = zeros(1, num_test);
  R = cell(1, num_test);
  for test_id = 1:num_test
    test_img = test_imgs{test_id};
    test_mask = test_masks{test_id};
    labels_img = test_model(model, test_img, test_mask);
    % Extract per pixel coordinates from the image segmentation
    r_labels = get_labels(labels_img);
    gt_labels = get_labels(gnd_truth{test_id});
    % Compute IOU on each labeled part
    num_gt_labels = numel(gt_labels);
    num_r_labels = numel(r_labels);
    gt_label_masks = cell(1, num_gt_labels);
    r_label_masks = cell(1, num_r_labels);
    % Precompute the label masks for each ground truth part
    for gt_label_id = 1:num_gt_labels
      gt_label = gt_labels{gt_label_id};
      gt_label_masks{gt_label_id} = plot_pixels({gt_label}, test_mask);
    end % end-for-gt_label_id
    % Precompute the label masks for each result part
    for r_label_id = 1:num_r_labels
      r_label = r_labels{r_label_id};
      r_label_masks{r_label_id} = plot_pixels({r_label}, test_mask);
    end % end-for-r_label_id
    % Compute IOU between each of the part label masks
    iou_scores_mat = zeros(num_gt_labels, num_r_labels);
    max_scores_mat = zeros(1, num_gt_labels);
    selected_ids = zeros(1, num_gt_labels);
    for gt_label_id = 1:num_gt_labels
      gt_label_mask = gt_label_masks{gt_label_id};
      for r_label_id = 1:num_r_labels
        r_label_mask = r_label_masks{r_label_id};
        score = iou(gt_label_mask, r_label_mask);
        iou_scores_mat(gt_label_id, r_label_id) = score;
      end % end-for-r_label_id
      % Select the max match for each part
      max_score = max(iou_scores_mat(gt_label_id, :));
      max_scores_mat(gt_label_id) = max_score; 
      selected_id = find(max_scores_mat == max_score);
      selected_ids(gt_label_id) = selected_id(1);
    end % end-for-gt_label_id
    mean_iou(test_id) = mean(max_scores_mat);
    R{test_id} = labels_img;
  end % end-for-test_id
end % end-function

function r = iou(A, B)
  sum_AB = double(A)+double(B);
  % Find the intersection of A and B
  intersect_AB = sum_AB;
  intersect_AB(intersect_AB ~= 2) = 0;
  intersect_AB(intersect_AB == 2) = 1;
  % Find the union of A and B
  union_AB = sum_AB;
  union_AB(union_AB ~= 0) = 1;
  % Intersection over union
  r = double(sum(intersect_AB(:)))/sum(union_AB(:));
end % end-function
