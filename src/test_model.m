function map = test_model(models0_poses, img, mask)
  if nargin == 0;
    A = load('horse1_example');
    % model0 = train_model(A.label);
    test_model(A.model0, A.img, A.msk);
    % model0 = train_model({horse_training_labels{1}});
    % test_model(model0, horse_training_images{1}, horse_training_masks{1});
  end

  % Set up internal parameters
  params = get_params('meaningful_parts');
  MAX_ITER = 2;
  if ~exist('mask', 'var') || isempty(mask)
    mask = img;
    % Set up image to prune out weaker pixels
    mask_arr = reshape(mask, 1, []);
    std_dev = std(double(mask_arr));
    max_value = max(mask_arr);
    strength_lim = max_value-(params.OUTLIER_MULT*std_dev);
    % Remove weak pixels
    mask(mask < strength_lim) = 0;
    % Generate our own mask
    mask(mask > 0) = 1;
  end % end-if-!exist
  dims = size(mask);
  preproc_tic = tic;
  % Get symmetry axis
  skeleton = symmetry_axis(mask, 'default');
  % Decompose the symmetry axis to segments, this is our initial grouping
  segments = decompose2segments(skeleton);
  % Put into a segment structure
  segment_struct = populate_segment_struct(segments, mask);
  segment_struct = sort_segment_struct(segment_struct, 'out_in');
  % Make sure every segment is mapped to some contour
  segment_struct = compose_by_no_contour(segment_struct);
  segment_struct = compose_by_protrusion(segment_struct, mask);
  segment_struct = compose_by_gradient(segment_struct, mask);
  % find protrusions in the mask
  [patch_assign, protru_bases, conf_plot] = conf_by_protrusion(segment_struct, mask);
  preproc_toc = toc(preproc_tic)
  figure; imagesc(conf_plot)
  hold on;
  yx_protru_bases = cell2mat(protru_bases(:));
  for i = 1:size(yx_protru_bases, 1)
    plot(yx_protru_bases(i, params.x), yx_protru_bases(i, params.y), 'ko', 'MarkerSize', 10);
  end
  hold off
  max_flow_tic = tic;
  % Find the center/'torso' of the object
  G = to_adj_graph(conf_plot, 3, 'masks');
  [max_flow_node, io_paths, verbose_paths] = max_flow_adj(G)
  max_flow_toc = toc(max_flow_tic)
  % Fit ellipses to the set of adjacent nodes to the max flow node
  labels = get_labels(conf_plot);
  size_mat = zeros(1, numel(labels));
  for label_id = 1:numel(labels)
    size_mat(label_id) = size(labels{label_id}, 1);
  end
  [~, order] = sort(size_mat, 'descend');
  order = order(1:2);
  if ~ismember(max_flow_node, order)
    [y_max, x_max] = size(mask);
    yx_center = [floor(y_max/2), floor(x_max/2)];
    for label_id = 1:numel(labels)
      if ismember(yx_center, labels{label_id}, 'rows')
        max_flow_node = label_id
      end
    end
  end
  adj_nodes = G(max_flow_node, :);
  adj_ids = find(adj_nodes == 1);
  num_nodes = numel(adj_ids);
  dcost_mat = zeros(1, num_nodes);
  optimal_fit_tic = tic;
  [~, mf_node_fit_cost] = fit_ellipse_model(labels{max_flow_node}, dims, MAX_ITER);
  for n_id = 1:num_nodes
    node_id = adj_ids(n_id);
    [~, node_fit_cost] = fit_ellipse_model(labels{node_id}, dims, MAX_ITER);
    fit_cost0 = node_fit_cost+mf_node_fit_cost;
    yx_merged = [labels{node_id}; labels{max_flow_node}];
    [~, fit_cost] = fit_ellipse_model(yx_merged, dims, MAX_ITER);
    dcost_mat(n_id) = fit_cost-fit_cost0;
  end
  optimal_fit_toc = toc(optimal_fit_tic)
  % Try to merge the torso with the parts adjacent
  merge_tic = tic;
  merge_nodes = find(dcost_mat < 0);
  num_merge = numel(merge_nodes);
  for m_id = 1:num_merge
    merge_id = adj_ids(merge_nodes(m_id));
    labels{max_flow_node} = [labels{max_flow_node}; labels{merge_id}];
    labels{merge_id} = [];
    % If label happen to be before the max flow node, then do a swap
    if merge_id < max_flow_node
      tmp = labels{max_flow_node};
      labels{max_flow_node} = labels{merge_id};
      labels{merge_id} = tmp;
      max_flow_node = merge_id;
    end
  end
  high_conf_plot = plot_pixels(labels, mask);
  % figure; imagesc(high_conf_plot)
  % Swap to the common max flow node as the one specified in model
  MAX_FLOW_NODE = 2;
  if max_flow_node ~= MAX_FLOW_NODE
    tmp = labels{MAX_FLOW_NODE};
    labels{MAX_FLOW_NODE} = labels{max_flow_node};
    labels{max_flow_node} = tmp;
    max_flow_node = MAX_FLOW_NODE;
  end
  labels(cellfun(@isempty, labels)) = [];
  high_conf_plot = plot_pixels(labels, mask);
  merge_toc = toc(merge_tic);
  % Refit the max flow node label
  refit_tic = tic;
  % figure; imagesc(high_conf_plot)
  labels = get_labels(high_conf_plot);
  num_labels = numel(labels);
  ellipse_models = cell(1, num_labels);
  for label_id = 1:num_labels
    yx_label = labels{label_id};
    [ellipse_model, ~] = fit_ellipse_model(yx_label, dims, MAX_ITER);
    ellipse_models{label_id} = ellipse_model;
  end
  refit_toc = toc(refit_tic);
  torso_model = ellipse_models{max_flow_node};
  norm_scale = 1.0/torso_model.long_axis;
  num_parts = numel(ellipse_models);
  % Normalize the size of each part with respect to the torso
  norm_models = cell(1, num_parts); 
  for part_id = 1:num_parts
    part_model = ellipse_models{part_id};
    models1{part_id} = norm3d(part_model, torso_model, norm_scale, dims);
  end % end-for-part_id
  % Retain original torso and all parts is with respect to original torso
  models1{max_flow_node}.a = torso_model.a;
  models1{max_flow_node}.long_axis = torso_model.long_axis;
  models1{max_flow_node}.X0_in = torso_model.X0_in;
  models1{max_flow_node}.Y0_in = torso_model.Y0_in;
  models1{max_flow_node}.phi = torso_model.phi;
  % Make everything with respect to ellipse model
  % models0 = models0_poses{1};
  % models0{max_flow_node}.a = models1{max_flow_node}.a;
  % models0{max_flow_node}.long_axis = models1{max_flow_node}.long_axis;
  % % Use ellipse model's ellipse center
  % models0{max_flow_node}.X0_in = models1{max_flow_node}.X0_in;
  % models0{max_flow_node}.Y0_in = models1{max_flow_node}.Y0_in;
  % Search the different orientations to find the best fit
  % nrots = 16;
  % nrow = nrots/2;
  % models0_rot = cell(1, nrots);
  % deg_mat = zeros(1, nrots);
  % z = size(mask);
  % for kr = 0:nrots-1;
  %   deg = floor(kr / nrots * 360);
  %   mrotate = deg2rotmatrix(deg);
  %   model_rot = xform_model(models0, max_flow_node, mrotate);
  %   % Store the rotated models
  %   models0_rot{kr+1} = model_rot;
  %   deg_mat(kr+1) = deg;
  % end
  % % Transform to the norm3d rotated model form
  % mrotate = deg2rotmatrix(0);
  % models1_rot = xform_model(models1, max_flow_node, mrotate);
  % % o = render_model(models1_rot, high_conf_plot, max_flow_node);
  % % figure; imagesc(o);
  % % Get centroids for all of the part fits
  % YX1 = zeros(num_parts, 2);
  % for m1_id = 1:num_parts
  %   model1 = models1_rot{m1_id};
  %   y1 = mean([model1.y1]);
  %   x1 = mean([model1.x1]);
  %   YX1(m1_id, :) = [y1, x1];
  % end
  % kd_tree = vl_kdtreebuild(YX1');
  % % Find the distance of head to closest part centroid
  % dist_mat = zeros(1, nrots);
  % for r = 1:nrots
  %   model0_rot = models0_rot{r};
  %   head_model0 = model0_rot{1};
  %   YX0 = [head_model0.y1, head_model0.x1];
  %   [~, dist] = vl_kdtreequery(kd_tree, YX1', YX0');
  %   dist_mat(r) = sqrt(dist);
  % end
  % % Find top 5 closest part fits to the model head position
  % [sorted, order] = sort(dist_mat, 'ascend');
  % num_k = 5;
  % k_index = order(1:num_k);
  % torso_model1 = models1_rot{max_flow_node};
  % torso_render1 = render_model({torso_model1}, z, 1);
  % err_mat = zeros(1, num_k);
  % for k = 1:num_k
  %   model0_rot = models0_rot{k_index(k)};
  %   torso_model0 = model0_rot{max_flow_node};
  %   torso_render0 = render_model({torso_model0}, z, 1);
  %   err_mat(k) = numel(find((torso_render0-torso_render1) ~= 0));
  %   [k_index(k), deg_mat(k_index(k)), err_mat(k)]
  % end
  % % Get the best orientation
  % orient_id = k_index(find(err_mat == min(err_mat)));
  % orient_model = models0_rot{orient_id};
  % num_parts = numel(orient_model);
  % id_mappings = containers.Map('KeyType', 'double', 'ValueType', 'any');
  % % Position ellipses to it's closest part
  % for part_id = 1:num_parts
  %   part_model = orient_model{part_id};
  %   y0 = mean([part_model.y1]);
  %   x0 = mean([part_model.x1]);
  %   YX0(part_id, :) = [y0, x0];
  %   [index, ~] = vl_kdtreequery(kd_tree, YX1', [y0, x0]');
  %   if ~id_mappings.isKey(index)
  %     id_mappings(index) = part_id;
  %   else
  %     id_mappings(index) = [id_mappings(index), part_id];
  %   end
  % end
  % orient_render = render_model(orient_model, z, max_flow_node);
  % figure; imagesc(orient_render)
  % num_regions = numel(models1_rot);
  % for region_id = 1:num_regions
  %   if id_mappings.isKey(region_id)
  %     part_ids = id_mappings(region_id);
  %     y0 = mean(YX0(part_ids, 1));
  %     x0 = mean(YX0(part_ids, 2));
  %     dy = YX1(region_id, 1)-y0;
  %     dx = YX1(region_id, 2)-x0;
  %     for p_id = 1:numel(part_ids)
  %       part_model = orient_model{part_ids(p_id)};
  %       for ell_id = 1:numel(part_model)
  %         part_model(ell_id).y1 = part_model(ell_id).y1+dy;
  %         part_model(ell_id).x1 = part_model(ell_id).x1+dx;
  %       end
  %       orient_model{part_ids(p_id)} = part_model;
  %     end
  %   end
  % end
  % orient_render = render_model(orient_model, z, max_flow_node);
  % figure; imagesc(orient_render)
  [pose_models, id_mappings] = approx_pose(models0_poses, models1, high_conf_plot, 16, max_flow_node);
  orient_model = pose_models.models;
  orient_render = render_model(orient_model, zeros(dims), max_flow_node);
  % run graph cuts part for pixel assignment
  if ~exist('opts_gc','var');
    opts_gc = struct; Q = load('modelFinal'); opts_gc.edge_model = Q.model;
  end
  E = edgesDetect(img, Q.model); ed = 0.75 * (1.0 - E);
  opts_gc.E = E;

  map = gclabel(orient_render, mask, img, opts_gc);
  figure; imagesc(map)
  %keyboard;

  % fig; imagex([double(mask)*double(max(max(map))), orient_render, map]);


  % figure; imagesc(orient_render);
  % Get the pixels of the rendering
  % ellipse_labels = get_labels(orient_render);
  % part_labels = cell(1, num_parts);
  % new_part_id = num_parts+1;
  % for label_id = 1:num_labels
  %   label_str = zeros(1, num_parts);
  %   label_tmp = cell(1, num_parts);
  %   label = labels{label_id};
  %   for part_id = 1:num_parts
  %     ellipse_label = ellipse_labels{part_id};
  %     label_tmp{part_id} = find(ismember(label, ellipse_label, 'rows'));
  %     label_str(part_id) = numel(label_tmp{part_id});
  %   end
  %   label_str = label_str/size(label, 1);
  %   if find(label_str > 0.60)
  %     majority = find(label_str == max(label_str));
  %     part_labels{majority} = [part_labels{majority}; label];
  %   else
  %     for part_id = 1:num_parts
  %       intrsct = label(label_tmp{part_id}, :);
  %       part_labels{part_id} = [part_labels{part_id}; intrsct];
  %     end
  %   end
    % if max(label_str) == 0
    %   part_labels{new_part_id} = label;
    %   new_part_id = new_part_id+1;
    % else
    %   max_label = find(label_str == max(label_str));
    %   part_labels{max_label} = [part_labels{max_label}; label];
    % end
  % end
  % % figure; imagesc(plot_pixels(part_labels, mask))
  % mask_pixels = cell2mat(labels(:));
  % assigned = cell2mat(part_labels(:)); 
  % unassigned = mask_pixels(find(~ismember(mask_pixels, assigned, 'rows')), :);
  % part_labels{end+1} = unassigned;
  % figure; imagesc(plot_pixels(part_labels, mask)) 

  % num_segments = numel(segment_struct);
  % segment_assign = zeros(1, num_parts);
  % new_part_id = num_parts+1;
  % for segment_id = 1:num_segments
  %   label_str = zeros(1, num_parts);
  %   masters = segment_struct(segment_id).master;
  %   master = cell2mat(masters(:));
  %   for part_id = 1:num_parts
  %     ellipse_label = ellipse_labels{part_id};
  %     label_str(part_id) = numel(find(ismember(master, ellipse_label, 'rows')));
  %   end
  %   if max(label_str) == 0
  %     segment_assign(segment_id) = new_part_id;
  %     new_part_id = new_part_id+1;
  %   else
  %     max_label = find(label_str == max(label_str));
  %     segment_assign(segment_id) = max_label(1);
  %   end
  % end
  % [segment_assign, segment_ids] = sort(segment_assign, 'ascend')
  % segment_struct1 = params.SEGMENT_STRUCT;
  % for i = 1:num_segments
  %   assign = segment_assign(i);
  %   segment_id = segment_ids(i);
  %   if numel(segment_struct1) < assign
  %     segment_struct1(assign) = segment_struct(segment_id);
  %     continue;
  %   end
  %   % Decide which should be the master, other one will be peripheral
  %   assign_master = segment_struct1(assign).master;
  %   segment_master = segment_struct(segment_id).master;
  %   if size(cell2mat(assign_master(:)), 1) > ...
  %      size(cell2mat(segment_master(:)), 1)
  %      master = segment_struct1(assign).master;
  %      peripheral = segment_struct(segment_id).master;
  %   else
  %      master = segment_struct(segment_id).master;
  %      peripheral = segment_struct1(assign).master;
  %   end
  %   % Merge the two structures
  %   segment_struct1(assign).master = master;
  %   segment_struct1(assign).peripherals = [segment_struct1(assign).peripherals, ...
  %                                          peripheral, ...
  %                                          segment_struct(segment_id).peripherals];
  %   segment_struct1(assign).p_endpoints = [segment_struct1(assign).p_endpoints, ...
  %                                          segment_struct(segment_id).p_endpoints];
  %   segment_struct1(assign).is_terminal = segment_struct1(assign).is_terminal || ...
  %                                         segment_struct(segment_id).is_terminal;
  %   segment_struct1(assign).contour = [segment_struct1(assign).contour; ...
  %                                      segment_struct(segment_id).contour];
  %   segment_struct1(assign).contour_plot = segment_struct1(assign).contour_plot+...
  %                                          segment_struct(segment_id).contour_plot;
  %   segment_struct1(assign).patch = [segment_struct1(assign).patch; ...
  %                                    segment_struct(segment_id).patch];
  % end
  % patches = {segment_struct1.patch};
  % figure; imagesc(plot_pixels(patches, mask))



end


function high_conf_plot = high_conf_assign(patches, patch_assign, mask, opt)
  if ~exist('opt', 'var') || isempty(opt)
    opt = 'default';
  end
  high_conf_plot = zeros(size(mask));
  num_patches = numel(patches);
  if strcmp(opt, 'show_low_conf')
    high_conf_plot = mask;
    high_conf_plot(high_conf_plot ~= 0) = num_patches+1;
  end
  % Plot the points 
  for p_id = 1:num_patches
    assignment = patch_assign{p_id};
    retained = find(assignment == 1);
    patch = patches{p_id};
    patch = patch(retained, :);
    patch_ind = sub2ind(size(mask), patch(:, 1), patch(:, 2));
    high_conf_plot(patch_ind) = p_id;
  end
end
