function cut_segments = decomp_by_gradient(segments)
  params = get_params('decomp_by_gradient');
  % Segments that have been decomposed by the change in gradient
  num_segments = numel(segments); 
  cut_segments = cell(1, num_segments);
  arr_pos = 1;
  % Iterate through all segments and break them up if necessary
  for segment_id = 1:num_segments
    curr_segment = segments{segment_id};
    num_pixels = size(curr_segment, 1);
    % Make sure we have enough pixels to get samples
    if num_pixels < params.MIN_SIZE
      cut_segments{arr_pos} = curr_segment;
      arr_pos = arr_pos+1;
      continue;
    end
    % Inner angles for each set of samples
    theta_mat = nan(1, num_pixels);
    % Traverse the pixels to get 2 vectors
    for p_id = params.START_POS:num_pixels-params.SAMPLE_SIZE
      % Since segments are all in the order of traversal
      START_A = p_id-params.SAMPLE_SIZE+1; END_A = p_id;
      START_B = p_id+params.SAMPLE_SIZE-1; END_B = p_id;
      % Get a sample of the segment
      vector_A = [curr_segment(START_A, :); curr_segment(END_A, :)];
      vector_B = [curr_segment(START_B, :); curr_segment(END_B, :)];
      theta = inner_angle(vector_A, vector_B);
      % Threshold our detections
      if theta < params.CUT_THRESHOLD
        theta_mat(p_id) = theta;
      end
    end % end-for-p_id
    % Locate the pixels that we can cut
    cut_candidates = find(~isnan(theta_mat));
    num_candidates = numel(cut_candidates);
    for cut_id = 1:num_candidates
      curr_cut = cut_candidates(cut_id);
      window = theta_mat(curr_cut-params.WINDOW_SPAN:curr_cut+params.WINDOW_SPAN);
      % Find the max of this window and do non-maximal suppression
      curr_max = min(window); % Smaller the angle, the sharper the curve
      max_pos = find(window == curr_max);
      window(setdiff(1:numel(window), max_pos)) = nan;
      theta_mat(curr_cut-params.WINDOW_SPAN:curr_cut+params.WINDOW_SPAN) = window;
    end % end-for-cut_id
    % Cut the segment at the maximal points found in the windows
    curr_position = 1;
    cut_locations = find(~isnan(theta_mat));
    num_locations = numel(cut_locations);
    % Cut the segment
    for loc_id = 1:num_locations
      cut_location = cut_locations(loc_id);
      cut_segment = curr_segment(curr_position:cut_location, :);
      cut_segments{arr_pos} = cut_segment;
      % Update curr_position to the cut location
      curr_position = cut_location;
      % Update the array position of the cut_segments cell array
      arr_pos = arr_pos+1;
    end % end-for-loc_id
    % Last cut must be the ending portion
    cut_segment = curr_segment(curr_position:end, :);
    cut_segments{arr_pos} = cut_segment;
    arr_pos = arr_pos+1; 
  end % end-for-segment_id
end % end-function
