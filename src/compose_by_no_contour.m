% compose_by_no_contour
%
% @param segment_struct struct containing segments and their associated data
% @return segment_struct struct where all segments with no contours are merged
% to those with an associated contour
%
% Given a set of segments, merge the segments that have no contour associated.
% These are generally segments that are small and lies between large segments
% serving as connecting component between the large segments.

function segment_struct = compose_by_no_contour(segment_struct)
  params = get_params('compose_by_no_contour');
  num_segments = numel(segment_struct);
  is_merged = false(1, num_segments);

  segment_masters = {segment_struct.master};
  segment_m_endpoints = {segment_struct.m_endpoints};
  segment_peripherals = {segment_struct.peripherals};
  segment_p_endpoints = {segment_struct.p_endpoints};
  segment_is_terminal = {segment_struct.is_terminal};
  segment_patches = {segment_struct.patch};

  % Find the segments that have no contour associated
  contours = {segment_struct.contour};
  no_contour = cellfun(@isempty, contours);
  terminal = [segment_struct.is_terminal];
  % Iterate through the segments to merge the ones without contours
  for curr_id = 1:num_segments
    % Skip if we have a contour associated, ignore terminals
    if ~no_contour(curr_id) || terminal(curr_id) || is_merged(curr_id)
      continue;
    end
    % Define a set of scores for segment affinity
    symm_axis_affinity_mat = zeros(1, num_segments);
    % Get the current segment
    curr_master = segment_masters{curr_id};
    curr_peripherals = segment_peripherals{curr_id};
    curr_segments = [curr_master, curr_peripherals];
    % Find the appropriate size to extract for sample
    for next_id = 1:num_segments
      % Sanity check
      if curr_id == next_id || terminal(next_id) || is_merged(next_id)
        continue;
      end
      next_master = segment_masters{next_id};
      next_peripherals = segment_peripherals{next_id};
      next_segments = [next_master, next_peripherals];
      % Get the samples from the two segments that connect
      [curr_sample, next_sample] = sample_segments(curr_segments, ...
                                                   next_segments, ...
                                                   params.NEIGHBOR_RADIUS, ...
                                                   params.STEP_SIZE, ...
                                                   params.THETA_THRESHOLD, ...
                                                   params.WINDOW_SPAN);
      % If they are connected, see if they should be merged
      if ~isempty(curr_sample) && ~isempty(next_sample)
        % Measure the inner angle of the samples
        theta = inner_angle(curr_sample, next_sample);
        symm_axis_affinity_mat(next_id) = theta;
      end
    end % end-for-next_id
    max_affinity = max(symm_axis_affinity_mat);
    max_id = find(symm_axis_affinity_mat == max_affinity);
    select_id = max_id(params.first);
    % Merge the current segment and its contours with that with max_id
    segment_masters{select_id} = [segment_masters{select_id}, segment_masters{curr_id}];
    segment_m_endpoints{select_id} = [segment_m_endpoints{select_id}, segment_m_endpoints{curr_id}];
    segment_peripherals{select_id} = [segment_peripherals{select_id}, segment_peripherals{curr_id}];
    segment_p_endpoints{select_id} = [segment_p_endpoints{select_id}, segment_p_endpoints{curr_id}];
    segment_is_terminal{select_id} = or(segment_is_terminal{select_id}, segment_is_terminal{curr_id});
    segment_patches{select_id} = [segment_patches{select_id}; segment_patches{curr_id}];
    % Mark as merged
    is_merged(curr_id) = true;
  end % end-for-curr_id
  % Update the segment_struct
  for segment_id = 1:num_segments 
    segment_struct(segment_id).master = segment_masters{segment_id};
    segment_struct(segment_id).m_endpoints = segment_m_endpoints{segment_id};
    segment_struct(segment_id).peripherals = segment_peripherals{segment_id};
    segment_struct(segment_id).p_endpoints = segment_p_endpoints{segment_id};
    segment_struct(segment_id).is_terminal = segment_is_terminal{segment_id};
    segment_struct(segment_id).patch = segment_patches{segment_id};
  end % end-for-segment_id
  merged_ids = find(is_merged == true);
  retained = setdiff(1:num_segments, merged_ids);
  segment_struct = segment_struct(retained);
end % end-function
