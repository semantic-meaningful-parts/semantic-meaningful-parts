function sample_size = size_sample(S, STEP_SIZE, THRESHOLD, WINDOW_SPAN)
  length_S = size(S, 1);
  % Inner angles for each set of samples
  theta_mat = nan(1, length_S);
  % Traverse the pixels to get 2 vectors
  for p_id = STEP_SIZE:length_S-STEP_SIZE
    START_A = p_id-STEP_SIZE+1; END_A = p_id;
    START_B = p_id+STEP_SIZE-1; END_B = p_id;
    % Get a sample of the segment
    vector_A = [S(START_A, :); S(END_A, :)];
    vector_B = [S(START_B, :); S(END_B, :)];
    theta = inner_angle(vector_A, vector_B);
    % Threshold our detections
    if theta < THRESHOLD
      theta_mat(p_id) = theta;
    end
  end % end-for-t
  % Locate the potential candidates with spikes in gradient
  candidates = find(~isnan(theta_mat));
  num_candidates = numel(candidates);
  for c_id = 1:num_candidates
    curr_pos = candidates(c_id);
    span_0 = max(1, curr_pos-WINDOW_SPAN);
    span_1 = min(length_S, curr_pos+WINDOW_SPAN);
    window = theta_mat(span_0:span_1);
    % Find the min of this window and do non-minimal suppression
    curr_min = min(window); % Smaller the angle, the sharper the curve
    min_pos = find(window == curr_min);
    window(setdiff(1:numel(window), min_pos)) = nan;
    theta_mat(span_0:span_1) = window;
  end % end-for-c_id
  % Invert the matrix values
  theta_mat = 180-theta_mat; % pi-theta for radians
  windows = find(~isnan(theta_mat));
  num_windows = numel(windows);
  % Look at the window size to determine stability
  stability = zeros(1, num_windows);
  % If there is no windows detected
  if isequal(num_windows, 0)
    sample_size = length_S; % We should simple take the entire 
    return;
  end
  % Iterate through the windows to compute their stability
  for w_id = 1:num_windows
    w_pos_0 = windows(w_id);
    theta_i = theta_mat(w_pos_0);
    % If we are on the last one window
    if isequal(w_id, num_windows)
      w_pos_1 = length_S; % select last element
    else
      w_pos_1 = windows(w_id+1); % select the start of next window
    end
    % Compute the stability in the curve
    dist = w_pos_1-w_pos_0;
    stability(w_id) = dist*theta_i;
  end % end-for-w_id
  max_stability = max(stability);
  w_pos = find(stability == max_stability);
  % If there was only 1 window
  if isequal(num_windows, 1)
    stability_0 = (windows(w_id)-1)*THRESHOLD;
    if stability_0 > max_stability
      w_pos = 1;
    end
  end
  sample_size = length_S-w_pos+1;
  [length_S, sample_size];
end

