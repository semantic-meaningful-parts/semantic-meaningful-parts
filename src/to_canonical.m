function to_canonical(img_labels)
  % Fit into a star-model: center, 4 sides, 2 top, 1 bot
  CANONICAL_STRUCT = struct('center', [], ...
                            'top', [], ...
                            'side', [], ...
                            'bot', []);
  % Preprocessing the labels
  label_ids = setdiff(unique(img_labels(:)), 0);
  num_labels = numel(label_ids);
  yx_labels = cell(1, num_labels);
  for l_id = 1:num_labels
    label_id = label_ids(l_id);
    ind_label = find(img_labels == label_id);
    yx_label = index2yx([1, 1; size(img_labels)], ind_label);
    yx_labels{l_id} = yx_label;
  end % end-for-l_id
  % Fit the set of ellipses onto the labels
  ellipse_models = cell(1, num_labels);
  for l_id = 1:num_labels
    % Extra the shape plot of the label
    shape = yx_labels{l_id};
    ellipse_models{l_id} = fit_ellipse_model(shape, size(img_labels), 3);
  end % end-for-l_id
  % Generate the n best fit ellipses for each part labels
  figure; imagesc(img_labels);
  hold on;
  for l_id = 1:num_labels
    ellipse_model = ellipse_models{l_id};
    num_ellipses = numel(ellipse_model);
    for ellipse_id = 1:num_ellipses
      X0 = ellipse_model(ellipse_id).X0_in;
      Y0 = ellipse_model(ellipse_id).Y0_in;
      major_axis = ellipse_model(ellipse_id).a;
      minor_axis = ellipse_model(ellipse_id).b;
      phi = ellipse_model(ellipse_id).phi;
      [X0, Y0; major_axis, minor_axis; phi, rad2deg(phi)]
      [x, y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);
      plot(x, y, 'ko', 'MarkerSize', 3);
    end
  end
  hold off;
  % Find the center node (torso)
  G = to_adj_graph(img_labels, 3, 'masks');
  [max_flow_node, io_paths, verbose_paths] = max_flow_adj(G)
  CANONICAL_STRUCT.center = ellipse_models{max_flow_node};
  % Find the neck and head, both sits 'above' center


  % Find the peripherals, sits 'below' the center 
  center_adj = G(max_flow_node, :);
  center_adj_ids = find(center_adj == 1)










 

  

 



end