function junctions = find_junctions(segment_sets, dims, DIST)
  junctions = containers.Map('KeyType', 'double', 'ValueType', 'any');
  num_sets = numel(segment_sets);
  % Iterate through each set of segment arrays containing segments
  endpoints_unique = [];
  endpoints_set = cell(1, num_sets);
  for set_id = 1:num_sets
    segments = segment_sets{set_id};
    num_segments = numel(segments);
    endpoints = [];
    % Look at each individual segment
    for segment_id = 1:num_segments
      segment = segments{segment_id};
      % Get the endpoints to all the segments
      endpoints = [endpoints; segment(1, :); segment(end, :)];
    end % end-for-segment_id
    endpoints = unique(endpoints, 'rows');
    endpoints_set{set_id} = endpoints;
    endpoints_unique = [endpoints_unique; endpoints];
  end % end-for-set_id
  endpoints_unique = unique(endpoints_unique, 'rows');
  num_unique = size(endpoints_unique, 1);
  adjacency_mat = zeros(num_sets, num_unique);
  % Populate the shortest distances from set endpoints to global endpoints
  for set_id = 1:num_sets
    s_endpoints = endpoints_set{set_id};
    kd_tree = vl_kdtreebuild(s_endpoints');
    for uniq_id = 1:num_unique
      u_endpoint = endpoints_unique(uniq_id, :);
      [~, dist] = vl_kdtreequery(kd_tree, s_endpoints', u_endpoint');
      % A given endpoint connects a component if dist < DIST
      if sqrt(dist) < DIST
        adjacency_mat(set_id, uniq_id) = 1;
      end
    end % end-for-uniq_id
  end % end-for-set_id
  % Sum up all columns 
  connectivity_mat = sum(adjacency_mat, 1);
  % Populate the junctions
  for uniq_id = 1:num_unique
    % We skip if only 1 point connected to the endpoint
    if connectivity_mat(uniq_id) == 1
      continue; % Occurs at endpoints, and previously merged junction
    end
    adjacency_list = adjacency_mat(:, uniq_id);
    segment_ids = find(adjacency_list == 1);
    yx_junction = endpoints_unique(uniq_id, :);
    key = yx2index([1, 1; dims], yx_junction);
    junctions(key) = segment_ids;
  end % end-for-uniq_id
end % end-function