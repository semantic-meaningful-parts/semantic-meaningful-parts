function [ellipse_model, model_cost] = fit_ellipse_model(shape, dims, MAX_ITER)
  params = get_params('ellipse_model');
  cost_mat = zeros(1, MAX_ITER);
  candidates = cell(1, MAX_ITER);
  % Get the major symmetry axis of this shape
  shape_plot = zeros(dims);
  shape_plot(yx2index([1, 1; dims], shape)) = 1;
  shape_plot = single_region(shape_plot);
  [y_shape, x_shape] = find(shape_plot == 1);
  shape = [y_shape, x_shape];
  skeleton = symmetry_axis(shape_plot, 'default');
  % Decompose skeleton into segments
  shape_axis = decompose2segments(skeleton);
  % If there are multiple shape axes
  if numel(shape_axis) > 1
    % We need to find the main branch in symmetry axis
    shape_axis = merge_simple(shape_axis, params.NEIGHBOR_RADIUS);  
    sizes = zeros(1, numel(shape_axis));
    for i = 1:numel(shape_axis)
      sizes(i) = size(shape_axis{i}, 1);
    end
    % Choose the major axis as our selected axis
    select_id = find(sizes == max(sizes));
    shape_axis = shape_axis{select_id};
  else % We have a single axis, so extract it from the cell
    shape_axis = shape_axis{1};
  end
  % Iterate through iterations to fit ellipses to shape
  for iter = 1:MAX_ITER
    % Perform k-cuts on the shape
    [shapes, shapes_axes, yx] = k_cuts(shape_axis, ...
                            shape, ...
                            iter-1, ... % 1st iteration should be 0-cut
                            params.STEP_SIZE, ...
                            params.WINDOW_SIZE);
    if isempty(shapes) % We could not find anymore locations to cut
      cost_mat(iter:MAX_ITER) = inf;
      break;
    end
    num_shapes = numel(shapes);
    ellipse_struct = repmat(params.ELLIPSE_STRUCT, 1, num_shapes);
    for shape_id = 1:num_shapes
      shape_i = shapes{shape_id};
      % Fit ellipse via least-squares
      ellipse_struct(shape_id) = fit_ellipse(shape_i(:, 2), shape_i(:, 1));
    end % end-for-shape_id
%     figure; imagesc(plot_pixels([shapes, shapes_axes], zeros(dims)));
%     hold on;
%     plot(yx(:, 1), yx(:, 2), 'ko', 'MarkerSize', 3);
%     hold off;
    candidates{iter} = ellipse_struct;
    % Calculate cost for each ellipse fit
    cost_mat(iter) = eval_cost(ellipse_struct, shape, dims);
  end % end-for-shape_id
  model_cost = min(cost_mat);
  select_id = find(cost_mat == model_cost);
  ellipse_model = candidates{select_id(1)};
end % end-function


function cost = eval_cost(ellipse_struct, shape, dims)
  num_ellipses = numel(ellipse_struct);
  ellipse_plot = zeros(dims);
  fit_cost = 0;
  % Plot the areas of the ellipse
  for e_id = 1:num_ellipses
    X0 = ellipse_struct(e_id).X0_in;
    Y0 = ellipse_struct(e_id).Y0_in;
    major_axis = ellipse_struct(e_id).a;
    minor_axis = ellipse_struct(e_id).b;
    phi = ellipse_struct(e_id).phi;
    if ~isempty(X0) && ~isempty(Y0)
      [X, Y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi, 'fill');
    end
    % Failure to fit ellipse
    if (isempty(X0) || isempty(Y0)) || ...
       (max(Y) > dims(1) || max(X) > dims(2))
      fit_cost = inf;
      fprintf('WARNING: Failed to fit ellipse onto region.\n');
      break;
    end
    ellipse_plot = ellipse_plot+plot_pixels({[Y, X]}, zeros(dims));
    fit_cost = fit_cost+numel(X);
  end % end-for-e_id
  % Plot the area of shape
  ellipse_plot(ellipse_plot ~= 0) = 1;
  shape_plot = plot_pixels({shape}, zeros(dims));
  coverage_plot = shape_plot-ellipse_plot;
  cov_cost = numel(find(coverage_plot ~= 0));
  % c = c_cov+sum(f_i) for i=[1,n] n ellipses 
  cost = cov_cost+fit_cost*num_ellipses;
end % end-function

function yx_plot = single_region(yx_plot)
  stats = regionprops(logical(yx_plot), 'Area', 'PixelIdxList');
  % If there are multiple regions (labeling errors)
  if numel(stats) > 1
    areas = cell2mat({stats.Area});
    pixel_list = {stats.PixelIdxList};
    max_region = find(areas == max(areas));
    for r_id = 1:numel(stats)
      if r_id == max_region
        continue;
      end
      yx_plot(pixel_list{r_id}) = 0;
    end
    [y, x] = find(yx_plot == 1);
    yx = [y, x];
    yx_plot = zeros(size(yx_plot));
    yx_plot(yx2index([1, 1,; size(yx_plot)], yx)) = 1;
  end
end % end-function
