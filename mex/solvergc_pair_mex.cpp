#include <math.h>
#include <matrix.h>
#include <mex.h>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <cmath>
#include <vector>
#include <limits> 
#include <cassert> 

// mex solvergc_pair.cpp

using namespace std;

#define eps 1e-8
#define MAT2C(x) ((x)-1)
#define C2MAT(x) ((x)+1)

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  if ((nrhs != 5) || (nlhs != 3)) {
    mexPrintf("Incorrect number of input/output arguments.\n");
    mexPrintf("USAGE: [pairs, weights, part_pairs] = solvergc_pair");
    mexPrintf("(part_ids, patch_ids, feat_yx, adjyx, BETA)\n");
    return;
  }
  if (!(mxIsInt32(prhs[0]) && mxIsInt32(prhs[1]) && mxIsInt32(prhs[2]))) { 
    mexPrintf("MAKE SURE that all input arguments are int (currently are not).\n");
    return;
  }

  // input processing
  int* part_ids = (int*)mxGetPr( prhs[0] );
  int* patch_ids = (int*)mxGetPr( prhs[1] );
  int* feat_yx = (int*)mxGetPr( prhs[2] );
  const mxArray* adjyx = prhs[3];
  float BETA = (float)mxGetScalar( prhs[4] );

  mwSize* sz = (mwSize*)mxGetDimensions( prhs[0] );
  int nnodes = sz[0];

  mwSize* sz_adj = (mwSize*)mxGetDimensions( prhs[3] );
  int ndims = mxGetNumberOfDimensions( prhs[3] );
  int rows = sz_adj[0];
  int cols = sz_adj[1];
  // cout << "rows: " << rows << "  cols: " << cols << "  ndims: " << ndims << endl;
  if (!((ndims==2) && (rows == cols))) { 
    mexPrintf("expected adj matrix map as fourth argument.\n");
    return;
  }
  int nparts = rows;

  // cout << "nparts: " << nparts << endl;
  // cout << "nnodes: " << nnodes << endl;

  // output
  vector<int> pairs_ni;
  vector<int> pairs_nj;
  vector<int> pairs_part_pi;
  vector<int> pairs_part_pj;
  vector<float> pairs_weights;

  //--------------------------------------------------------------------------
  // lift off
  //--------------------------------------------------------------------------
  int ndeforms = 15;

  const mxArray *cell;


  // mwSize ncell = mxGetNumberOfElements(adjyx);
  // cout << "ncell: " << ncell << endl;
  // for (mwSize i = 0; i < ncell; ++i) {
  //   cell = mxGetCell(adjyx, i);
  //   cout << "cell(" << i << "): ";
  //   // if (mxIsEmpty(cell)) {
  //   if (cell == NULL) {
  //     cout << "EMPTY!";
  //   } else {
  //     if (mxIsDouble(cell)) {
  //       mwSize n = mxGetNumberOfElements(cell);
  //       double* pr = mxGetPr(cell);
  //       for (mwSize j = 0; j < floor(n/2); ++j) {
  //         cout << pr[j] << " ";
  //       }
  //       cout << endl << "          ";
  //       for (mwSize j = 0; j < floor(n/2); ++j) {
  //         cout << pr[ndeforms + j] << " ";
  //       }
  //       // cout << endl;
  //     }
  //   }
  //   cout << endl;
  // }
  // return;


  for (int ni = 0; ni < nnodes; ++ni) {
    int pidx = MAT2C(part_ids[ni]);
    int ppidx = MAT2C(patch_ids[ni]);

    for (int nj = ni + 1; nj < nnodes; ++nj) {
      int pjdx = MAT2C(part_ids[nj]);
      mwIndex cidx = pjdx * nparts + pidx;
      cell = mxGetCell(adjyx, cidx);

      // if (!mxIsEmpty(cell)) {
      if (cell != NULL) {
        double* yxs = (double*) mxGetPr(cell);
        if (yxs == NULL) { continue; } // emergency but shouldn't happen unless i was manually editing adjyx


        // 1. Find pj on pi
        // cout << "HEY5: yxs, ppidx = " << yxs << " " << ppidx << endl;
        int rpi_jy = MAT2C(yxs[ppidx]);
        // cout << "HEY6: rpi_jy = " << rpi_jy << endl;
        int rpi_jx = MAT2C(yxs[ndeforms+ppidx]);
        // cout << "HEY7: rpi_jx = " << rpi_jx << endl;

        int pi_jy = rpi_jy + MAT2C(feat_yx[ni]);
        // cout << "HEY8: pi_jy = " << pi_jy << endl;
        int pi_jx = rpi_jx + MAT2C(feat_yx[ni+nnodes]);
        // cout << "HEY9: pi_jx = " << pi_jx << endl;

        // 2. Find pi on pj
        mwIndex cjdx = pidx * nparts + pjdx;
        // mwIndex cjdx = pjdx * nparts + pidx;
        // cout << "HEY10" << endl;
        cell = mxGetCell(adjyx, cjdx);
        // cout << "HEY11" << endl;
        yxs = (double*) mxGetPr(cell);
        // cout << "HEY12" << endl;

        int ppjdx = MAT2C(patch_ids[nj]);
        // cout << " (nj,pjdx,ppjdx) = " << nj << "," << pjdx << "," << ppjdx << endl;

        int rpj_iy = MAT2C(yxs[ppjdx]);
        int rpj_ix = MAT2C(yxs[ndeforms+ppjdx]);

        int pj_iy = rpj_iy + MAT2C(feat_yx[nj]);
        int pj_ix = rpj_ix + MAT2C(feat_yx[nj+nnodes]);

        // compute distance
        // cout << " (ni,pidx,ppidx) = " << ni << "," << pidx << "," << ppidx << endl;
        // cout << " (nj,pjdx,ppjdx) = " << nj << "," << pjdx << "," << ppjdx << endl;
        // cout << "rpj_i: (" << rpj_iy << "," << rpj_ix << ")" << endl;
        // cout << "rpi_j: (" << rpi_jy << "," << rpi_jx << ")" << endl;
        // cout << "pi_j: (" << pi_jy << "," << pi_jx << ")" << endl;
        // cout << "pj_i: (" << pj_iy << "," << pj_ix << ")" << endl;
        double dy = (double) pi_jy - (double) pj_iy;
        double dx = (double) pi_jx - (double) pj_ix;
        double d = dy*dy + dx*dx;
        // double d = sqrt((double) dy*dy + dx*dx);
        double s = exp(-BETA * d);

        // add to list of pairwise weights
        pairs_weights.push_back(s);
        pairs_ni.push_back(C2MAT(ni));
        pairs_nj.push_back(C2MAT(nj));
        pairs_part_pi.push_back(C2MAT(pidx));
        pairs_part_pj.push_back(C2MAT(pjdx));
      }
    }
  }

  // do output
  int npairs = pairs_weights.size();
  // cout << "npairs: " << npairs << endl;
  // cout << "BETA: " << BETA << endl;
  // plhs[0] = mxCreateDoubleMatrix(npairs, 2, mxREAL); // pairs
  // plhs[1] = mxCreateDoubleMatrix(npairs, 1, mxREAL); // weights
  // plhs[2] = mxCreateDoubleMatrix(npairs, 2, mxREAL); // pairs_part
  mwSize odims[2]; odims[0] = npairs; odims[1] = 2;
  plhs[0] = mxCreateNumericArray(2, odims, mxINT32_CLASS, mxREAL); // pairs
  plhs[2] = mxCreateNumericArray(2, odims, mxINT32_CLASS, mxREAL); // pairs_part
  odims[1] = 1;
  plhs[1] = mxCreateNumericArray(2, odims, mxDOUBLE_CLASS, mxREAL); // weights
  int* pairs_ij = (int*)mxGetPr(plhs[0]);
  double* pairs_w = (double*)mxGetPr(plhs[1]);
  int* pairs_ninj = (int*)mxGetPr(plhs[2]);

  for (int k = 0; k < npairs; ++k) {
    pairs_ij[k] = (int) pairs_ni[k];
    pairs_ij[k+npairs] = (int) pairs_nj[k];
    pairs_w[k] = (double) pairs_weights[k];
    pairs_ninj[k] = (int) pairs_part_pi[k];
    pairs_ninj[k+npairs] = (int) pairs_part_pj[k];
  }
}

