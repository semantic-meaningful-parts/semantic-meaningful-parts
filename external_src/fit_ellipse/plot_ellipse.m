%function [X,Y] = plot_ellipse(varargin) 
function [X,Y] = plot_ellipse(x0, y0, a, b, angle, opt) 
%# This functions returns points to draw an ellipse 
%# 
%# @param x0 X coordinate 
%# @param y0 Y coordinate 
%# @param a Semimajor axis 
%# @param b Semiminor axis 
%# @param angle Angle of the ellipse (in rad) 
%# 
% Source: http://stackoverflow.com/questions/2153768/draw-ellipse-and-ellipsoid-in-matlab/24531259#24531259 
% Modified by Christian Fässler

steps = 360; 

% if nargin == 1 || nargin == 2 
% x = varargin{1}.X0_in; 
% y = varargin{1}.Y0_in; 
% a = varargin{1}.a; 
% b = varargin{1}.b; 
% angle = varargin{1}.phi; 
% if nargin == 2 
% steps = varargin{2}; 
% end 
% else if nargin == 5 || nargin == 6 
% x = varargin{1}; 
% y = varargin{2}; 
% a = varargin{3}; 
% b = varargin{4}; 
% angle = varargin{5}; 
% if nargin == 6 
% steps = varargin{6}; 
% end 
% else 
% error('Wrong input'); 
% end 
% end 

if ~exist('opt', 'var') || isempty(opt)
  opt = 'default';
end

beta = -angle; 
sinbeta = sin(beta); 
cosbeta = cos(beta);

alpha = linspace(0, 2*pi, steps)'; 
sinalpha = sin(alpha); 
cosalpha = cos(alpha);

X = round(x0 + (a * cosalpha * cosbeta - b * sinalpha * sinbeta)); 
Y = round(y0 + (a * cosalpha * sinbeta + b * sinalpha * cosbeta));

if strcmp(opt, 'fill')
  x_min = min(X); x_max = max(X);
  y_min = min(Y); y_max = max(Y);
  yx_mat = zeros([y_max, x_max]);
  % try
  k = convhull(Y, X); % create convexhull
  % catch e; fprintf('convhull issue\n'); keyboard; end;
  [y, x] = ndgrid(1:y_max,1:x_max); % create index space
  [in_poly, on_poly] = inpolygon(y, x, Y(k), X(k)); % create filled-in matrix:
  yx_poly = [[y(in_poly), x(in_poly)]; [y(on_poly), x(on_poly)]];
  yx_mat(sub2ind([y_max, x_max], yx_poly(:, 1), yx_poly(:, 2))) = 1;
  [Y, X] = find(yx_mat == 1);
end

if nargout==1, X = [X Y]; end 
end
