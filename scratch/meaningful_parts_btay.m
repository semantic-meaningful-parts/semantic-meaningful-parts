% meaningful_patches
%
% @param dataset dataset containing images
% @param sample_id sample index within the dataset
% @param class_id class label for the particular sample
% @return patch_info struct containing the meaningful parts extracted
% from the image
%
% Given an image from a dataset, apply symmetry axis transform and 
% decompose the axis into a set of parts by junctions. Extract the image
% patches corresponding to the parts. We denote these patches as 
% meaningful patches.

function patch_struct = meaningful_parts(img, mask)
  % Set up internal parameters
  params = get_params('meaningful_parts');
  opts = struct; Q = load('modelFinal'); opts.edge_model = Q.model;
  E = edgesDetect(img, Q.model);
  ed = 0.75 * (1.0 - E);

  % If we are not given the mask
  if ~exist('mask', 'var') || isempty(mask)
    mask = img;
    % Set up image to prune out weaker pixels
    mask_arr = reshape(mask, 1, []);
    std_dev = std(double(mask_arr));
    max_value = max(mask_arr);
    strength_lim = max_value-(params.OUTLIER_MULT*std_dev);
    % Remove weak pixels
    mask(mask < strength_lim) = 0;
    % Generate our own mask
    mask(mask > 0) = 1;
  end % end-if-!exist
  % Total time start
  time_start = tic;
  % Get symmetry axis
  sa_start = tic;
  skeleton = symmetry_axis(mask, 'default');
  sa_elapsed = toc(sa_start);
  % Decompose the symmetry axis to segments, this is our initial grouping
  d2s_start = tic;
  segments = decompose2segments(skeleton);
  d2s_elasped = toc(d2s_start);

  % place code here brian
  TESTBTAY = 0;
  if TESTBTAY; 
    segments_0 = segments;
    segment_struct_0 = populate_segment_struct(segments_0, mask);
    [sp_plot_0, sa_plot_0] = plot_segment_struct(segment_struct_0, mask, 'overlay');
  end
  % segments = decomp_by_edge_dist(segments, mask);
  segment_struct = populate_segment_struct(segments, mask);
  if TESTBTAY;
    [sp_plot, sa_plot] = plot_segment_struct(segment_struct, mask, 'overlay');
  end
  
  segment_struct = populate_segment_struct(segments, mask);
  segment_struct = sort_segment_struct(segment_struct, 'out_in');

  segment_struct = compose_by_no_contour(segment_struct);
  [sp_plot, sa_plot] = plot_segment_struct(segment_struct, mask, 'overlay');
  fig(9); imagesc([sp_plot, sa_plot]); title('Results after Compose by No Contour');

  segment_struct = compose_by_protrusion(segment_struct, mask);
  [sp_plot, sa_plot] = plot_segment_struct(segment_struct, mask, 'overlay');
  fig(10); imagesc([sp_plot, sa_plot]); title('Results after Compose by Protrusion');

  if TESTBTAY;
    [sp_plot_2, sa_plot_2] = plot_segment_struct(segment_struct, mask, 'overlay');
    fig(100); imagesc([sp_plot_0, sp_plot, sp_plot_2]); 
    title('parts og | parts edge dist decomp | parts composed');
    fig(101); imagesc([sa_plot_0, sa_plot, sa_plot_2]);
    title('axes og | axes edge dist decomp | axes composed');
    keyboard;
  end

  segment_struct = compose_by_gradient(segment_struct, mask);
  [sp_plot, sa_plot] = plot_segment_struct(segment_struct, mask, 'overlay');
  fig(11); imagesc([sp_plot, sa_plot]); title('Results after Compose by Gradient');

  [patch_assign, protru_bases] = protrusion_partition(segment_struct, mask);


  yx_protru_bases = cell2mat(protru_bases(:));
  fig(12); imagesc(sp_plot); title('Results after Protrusion Base');
  hold on;
  for i = 1:size(yx_protru_bases, 1)
    plot(yx_protru_bases(i, params.x), yx_protru_bases(i, params.y), 'ko', 'MarkerSize', 10);
  end
  hold off;

  patches = {segment_struct.patch};
  num_patches = numel(patches);
  sp_plot = zeros(size(mask));
  for p_id = 1:num_patches
    assign = patch_assign{p_id};
    retained = find(assign == 1);
    patch = patches{p_id};
    patch = patch(retained, :);
    patch_ind = sub2ind(size(mask), patch(:, 1), patch(:, 2));
    sp_plot(patch_ind) = p_id;
  end
  fig(13); imagesc(sp_plot);

  % run final graph cuts part
  opts.E = E;
  opts.patch_mask = sp_plot;
  % [pp,contours,pm] = assign2parts_gc(segments, mask, img, opts);
  [pp,contours,pm] = assign2parts_gc(segment_struct, mask, img, opts);
  fig(105); imagesc(pm);

  % [sp_plot, sa_plot] = plot_segment_struct(segment_struct, mask, 'overlay');
  % figure; imagesc(sp_plot);
  % figure; imagesc(sa_plot);

  %% TODO: btay note: I'm unclear on the compose2parts and parts2patches call after this line. Not sure if this is way out of date or not. 

  % Takes the segments and joins them into parts
  c2p_start = tic;
  parts = compose2parts(segments, mask);
  c2p_elasped = toc(c2p_start);
  % Extract the patches and weights corresponding to the parts
  p2p_start = tic;
  [patches, patch_weights, masks_pixels, centroids] = parts2patches(parts, ...
                                                                    img, ...
                                                                    mask, ...
                                                                    params.PARTS2PATCHES_OPT);
  p2p_elapsed = toc(p2p_start);
  % Generate connectivity structure for each part and its junctions 
  gj_start = tic;
  junctions = get_junctions(parts, img);
  gj_elasped = toc(gj_start);

  %lp_start = tic;
  %label_parts(parts, junctions, mask);
  %lp_elasped = toc(lp_start);

  % Get relative parts centered at [0,0]
  grp_start = tic;
  relative_parts = batch_get_relative_part(parts);
  grp_elasped = toc(grp_start);
  % Get relative junctions centered at [0,0]
  grj_start = tic;
  relative_junctions = batch_get_relative_junctions(junctions, parts, size(mask));
  grj_elasped = toc(grp_start);
  % Populate the meaningful patch structure with data
  patch_struct = populate_patch_struct(parts, ...
                                       patches, ...
                                       patch_weights, ...
                                       junctions, ...
                                       relative_parts, ...
                                       relative_junctions, ...
                                       masks_pixels, ...
                                       centroids);
  % Total time elapsed
  time_elasped = toc(time_start);

  if params.VISUAL
    semantic_mask = zeros(size(mask));
    num_parts = numel(parts);
    % Generate the semantic mask
    for part_id = 1:num_parts
      mask_pixels = masks_pixels{part_id};
      mask_pixels_ind = sub2ind(size(mask), mask_pixels(:, 1), mask_pixels(:, 2));
      semantic_mask(mask_pixels_ind) = part_id;
    end
    num_patches = numel(patches)*2;
    num_figures = num_patches+6;
    num_cols = 4;
    num_rows = ceil(num_figures/num_cols);
    figure;
    % Plot important figures of step progression
    subplot(num_rows, num_cols, 1); imagesc(img);
    subplot(num_rows, num_cols, 2); imagesc(mask);
    subplot(num_rows, num_cols, 3); imagesc(skeleton);
    subplot(num_rows, num_cols, 4); imagesc(plot_pixels(segments, mask, 'overlay'));
    subplot(num_rows, num_cols, 5); imagesc(plot_pixels(parts, mask, 'overlay'));
    subplot(num_rows, num_cols, 6); imagesc(semantic_mask);
    % Add centroid markers to semantic mask
    for part_id = 1:num_parts
      hold on;
      yx = centroids{part_id};
      plot(yx(params.x), yx(params.y), 'ko', 'MarkerSize', 5);
      hold off;
    end
    % Plot patches and patch weights
    img_idx = 1;
    for fig_idx = 7:num_figures
      if mod(fig_idx, 2) == 1
        subplot(num_rows, num_cols, fig_idx); imagesc(patches{img_idx});
      else
        subplot(num_rows, num_cols, fig_idx); imagesc(patch_weights{img_idx});
        img_idx = img_idx+1;
      end
    end
  end

  if params.VOCAL
    fprintf('Elasped time for extracting symmetry axis: %.3f seconds\n', ...
      sa_elapsed);
    fprintf('Elasped time for decomposing to segments: %.3f seconds\n', ...
      d2s_elasped);
    fprintf('Elasped time for composing segments to parts: %.3f seconds\n', ...
      c2p_elasped);
    fprintf('Elasped time for extracting patches and weights: %.3f seconds\n', ...
      p2p_elasped);
    fprintf('Elasped time for getting the junctions: %.3f seconds\n', ...
      gj_elasped);
    fprintf('Elasped time for generating relative parts: %.3f seconds\n', ...
      grp_elasped);
    fprintf('Elasped time for generating relative junctions: %.3f seconds\n', ...
      grj_elasped);

    fprintf('Total time incurred: %.3f seconds\n', ...
      time_elasped); 
  end
end % end-function

% Helper function
% Run get_relative_part in batch
function relative_parts = batch_get_relative_part(parts)
  num_parts = numel(parts);
  relative_parts = cell(1, num_parts);
  for part_id = 1:num_parts
    part = parts{part_id};
    relative_part = get_relative_part(part);
    relative_parts{part_id} = relative_part;
  end % end-for-part_id
end % end-function

% Helper function
% Run get_relative_junctions in batch
function relative_junctions = batch_get_relative_junctions(junctions, parts, yx_dim)
  num_parts = numel(parts);
  relative_junctions = cell(1, num_parts);
  for part_id = 1:num_parts
    part = parts{part_id};
    connections = junctions{part_id};
    relative_connections = get_relative_junctions(connections, part, yx_dim);
    relative_junctions{part_id} = relative_connections;
  end % end-for-part_id
end % end-function


% Helper function
% Populates segment structure with data
function segment_struct = populate_segment_struct(segments, mask)
  params = get_params('meaningful_parts');
  segment_struct = params.SEGMENT_STRUCT;
  num_segments  = numel(segments);
  % Assign some part of the contour of the mask to the segments
  contours = assign2parts(segments, mask, 'edge');
  patches = assign2parts(segments, mask, 'full');
  % Check if each segment contains a terminal point
  complexities = joint_complexity(segments);
  terminals = is_terminal(complexities);
  % Iterate through each segment and populate the segment struct
  for segment_id = 1:num_segments
    segment = segments{segment_id};
    contour = contours{segment_id};
    patch = patches{segment_id};
    segment_struct(segment_id).master = {segment};
    segment_struct(segment_id).m_endpoints = [segment(params.first, :); segment(end, :)];
    segment_struct(segment_id).peripherals = {}; % Initialize with no peripherals
    segment_struct(segment_id).p_endpoints = {}; % Hence no peripheral endpoints
    segment_struct(segment_id).is_terminal = terminals(segment_id);
    segment_struct(segment_id).contour = contour;
    segment_struct(segment_id).contour_plot = plot_pixels({contour}, mask);
    segment_struct(segment_id).patch = patch; 
  end % end-for-segment_id
end % end-function


% Helper function
% Sorts the segments 
function segment_struct = sort_segment_struct(segment_struct, opt)
  if ~exist('opt', 'var') || isempty(opt)
    opt = 'out_in'
  end
  opt = lower(opt);
  num_segments = numel(segment_struct);
  terminals = [segment_struct.is_terminal];
  % First find the set of terminal ids
  terminal_ids = find(terminals == true);
  general_ids = setdiff(1:num_segments, terminal_ids);
  % Parition our segments and complexities into terminal and genearl set
  terminal_segments = segment_struct(terminal_ids);
  general_segments = segment_struct(general_ids);
  if strcmp(opt, 'out_in')
    % Sort by merging the sets such that we process terminals first
    segment_struct = [terminal_segments, general_segments];
  elseif strcmp(opt, 'in_out')
    % Sort by merging the sets such that we process non-terminals first
    segment_struct = [general_segments, terminal_segments];
  end
end % end-function


% Helper function
% Populates meaningful patch structure with data
function patch_struct = populate_patch_struct(parts, ...
                                              patches, ...
                                              patch_weights, ...
                                              junctions, ...
                                              relative_parts, ...
                                              relative_junctions, ...
                                              masks_pixels, ...
                                              centroids)
  % Set up internal parameters
  params = get_params('meaningful_parts');
  patch_struct = params.PATCH_STRUCT;
  num_parts = numel(parts);
  for part_id = 1:num_parts
    patch_struct(part_id).part = parts{part_id};
    patch_struct(part_id).patch = patches{part_id};
    patch_struct(part_id).patch_weights = patch_weights{part_id};
    patch_struct(part_id).junctions = junctions{part_id};
    patch_struct(part_id).relative_part = relative_parts{part_id};
    patch_struct(part_id).relative_junctions = relative_junctions{part_id};
    patch_struct(part_id).part_mask = masks_pixels{part_id};
    patch_struct(part_id).centroid = centroids{part_id};
  end % end-for-part_id
end % end-function
