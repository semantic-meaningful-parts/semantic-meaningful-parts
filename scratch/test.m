% Test difference between using telea and default methods
for id = 1:1
  % Load image and mask
  img = horse_testing_images_bw_seg{id};
  mask = horse_training_masks{id};
  % Get symmetry axes
  symm_axis_default = symmetry_axis(mask, 'default');
  symm_axis_telea = symmetry_axis(mask, 'telea');
  % Decompose symmetry axes into segments
  segments_default = decompose2segments(symm_axis_default);
  segments_telea = decompose2segments(symm_axis_telea);
  % Decompose the segments if the change in gradient is large
  decomp_grad_segments_default = decomp_by_gradient(segments_default);
  decomp_grad_segments_telea = decomp_by_gradient(segments_telea);

  %%%
  % Completed decomposition steps
  %%%

  % Sort the decomposed segments
  % decomp_segments_default = sort_segments(decomp_grad_segments_default, 'out_in');
  % decomp_segments_telea = sort_segments(decomp_grad_segments_telea, 'out_in');
  % After decomposing them, we sort the segments from out to in
  % segment_struct_default = populate_segment_struct(segments, mask);
  % segment_struct_telea = populate_segment_struct(decomp_segments_telea, )
  % segment_struct = compose_by_no_contour(segment_struct);


  % Show the difference between telea and default methods
  figure; 
  subplot(2, 4, 1); imagesc(img);
  subplot(2, 4, 2); imagesc(symm_axis_default);
  subplot(2, 4, 3); imagesc(plot_pixels(segments_default, mask, 'overlay'));
  subplot(2, 4, 4); imagesc(plot_pixels(decomp_grad_segments_default, mask, 'overlay'));
  subplot(2, 4, 5); imagesc(mask);
  subplot(2, 4, 6); imagesc(symm_axis_telea);
  subplot(2, 4, 7); imagesc(plot_pixels(segments_telea, mask, 'overlay'));
  subplot(2, 4, 8); imagesc(plot_pixels(decomp_grad_segments_telea, mask, 'overlay'));
end








% horse_test_labels_dir = 'data/horse_cow/horse/test/part/';
% horse_train_labels_dir = 'data/horse_cow/horse/train/part/';
% cow_train_labels_dir = 'data/horse_cow/cow/train/part/';
% cow_test_labels_dir = 'data/horse_cow/cow/test/part/';

% labels_dir = cow_test_labels_dir;
% part_labels = cow_testing_labels;

% labels_files = dir(labels_dir);
% num_files = numel(labels_files);
% file_ids = [];

% for file_idx = 1:num_files
%   if ~strcmp(labels_files(file_idx).name(1), '.')
%     file_ids = [file_ids, file_idx];
%   end
% end
% labels_files = labels_files(file_ids);
% num_files = numel(part_labels);
% cd 'selected_parts'
% for i = 1:num_files
%   labels = part_labels{i};
%   labels(labels > 0) = (labels(labels > 0)*10+100);
%   imwrite(labels, labels_files(i).name);
% end
