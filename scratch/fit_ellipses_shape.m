tic;
labels_plot = horse_training_labels{1};
label_ids = setdiff(unique(labels_plot(:)), 0);
num_labels = numel(label_ids);

G = labels_adj_graph(labels_plot, 3)

% Extract the labels
labels = cell(1, num_labels);
for i = 1:num_labels
  label_id = label_ids(i);
  ind_label = find(labels_plot == label_id);
  yx_label = index2yx([1, 1; size(labels_plot)], ind_label);
  labels{label_id} = yx_label;
end
% Define ellipse struct
ellipse_struct = struct('a', [],... // radius of major axis
                        'b', [],... // radius of minor axis
                        'phi', [],... // angle  (w.r.t. image plane)
                        'X0', [],... // center
                        'Y0', [],...
                        'X0_in', [],... // but use this center
                        'Y0_in', [],...
                        'long_axis', [],... // diameter of major axis
                        'short_axis', [],... // diameter of minor axis
                        'status', '');
% Generate ellipses for each label
for label_id = 1:num_labels
  yx_label = labels{label_id};
  ellipse_struct(label_id) = fit_ellipse(yx_label(:, 2), yx_label(:, 1));
end
% Generate best fit single ellipse for each part label
figure; imagesc(labels_plot);
hold on;
for label_id = 1:num_labels
  X0 = ellipse_struct(label_id).X0_in;
  Y0 = ellipse_struct(label_id).Y0_in;
  major_axis = ellipse_struct(label_id).a;
  minor_axis = ellipse_struct(label_id).b;
  phi = ellipse_struct(label_id).phi;
  [x, y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);
  plot(x, y, 'ko', 'MarkerSize', 3);
end
hold off;
% Fit multiple ellipses based on coverage and fit cost
ellipse_models = cell(1, num_labels);
for label_id = 1:num_labels
  % Extra the shape plot of the label
  shape = labels{label_id};
  ellipse_models{label_id} = fit_ellipse_model(shape, size(labels_plot), 3);
end
% Generate the n best fit ellipses for each part labels
figure; imagesc(labels_plot);
hold on;
for label_id = 1:num_labels
  ellipse_model = ellipse_models{label_id};
  num_ellipses = numel(ellipse_model);
  for ellipse_id = 1:num_ellipses
    X0 = ellipse_model(ellipse_id).X0_in;
    Y0 = ellipse_model(ellipse_id).Y0_in;
    major_axis = ellipse_model(ellipse_id).a;
    minor_axis = ellipse_model(ellipse_id).b;
    phi = ellipse_model(ellipse_id).phi;
    [x, y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);
    plot(x, y, 'ko', 'MarkerSize', 3);
  end
end
hold off;

toc(tic)
