%----------------------------------------------------------------------------%
% username.m
%
% gets username of person running the code
%----------------------------------------------------------------------------%
function name = username 
name = getenv('USER');
end
