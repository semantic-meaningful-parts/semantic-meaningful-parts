% converts a label image to a set of nlabels x binary indicator maps
% (thus far, only works for 2D images)
%
% @param: label_map : M x N label image with labels 1...L
% @return: bin_maps : M x N x L binary indicator maps with "1" where 
%   the corresponding label would appear in the label_map

function [bin_maps, labels] = explode(label_map, labels)

if length(size(label_map)) ~= 2; fprintf('NOPE\n'); keyboard; end;

if ~exist('labels','var');
  labels = unique(label_map(:));
end
L = length(labels);
[rows, cols, ~] = size(label_map);

bin_maps = true(rows, cols, L);
for k = 1:L;
  bin_maps(:, :, k) = logical(label_map == labels(k));
end
end



% % % %%% TODO: doesn't work for nonadjacent labels :( :( :( :( :(
% % % function [bin_maps, labels] = explode(label_map)
% % % 
% % % if length(size(label_map)) ~= 2; fprintf('NOPE\n'); keyboard; end;
% % % 
% % % labels = unique(label_map(:));
% % % L = length(labels);
% % % % L = max(label_map(:));
% % % [rows, cols, ~] = size(label_map);
% % % npxls = rows * cols;
% % % pxl_inds = 1:npxls;
% % % 
% % % inds = pxl_inds(:) + label_map(:) * npxls;
% % % 
% % % bin_maps = zeros(rows, cols, L+1); % to save space use uint8
% % % bin_maps(inds) = 1;
% % % end