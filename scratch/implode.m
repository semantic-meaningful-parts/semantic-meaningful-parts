% converts a set of nlabels x binary indicator maps to a label image
% (thus far, only works for 2D images)
%
% @param: bin_maps : M x N x L binary indicator maps with "1" where 
%   the corresponding label would appear in the label_map
% @return: label_map : M x N label image with labels 1...L

function label_map = implode(bin_maps, lind)
bin_maps = round(bin_maps);
sz = size(bin_maps);
nsz = length(sz);
b = bin_maps(:);

% fail cases
if (nsz ~= 3) ...
  && ((nsz == 2) && (max(b) > 1 || min(b) < 0));
  fprintf('implode: NOPE\n');
  keyboard; 
end;

[~, ~, L] = size(bin_maps);

if ~exist('lind', 'var'); lind = 1:L; end;
lind = reshape(lind, [1, 1, L]);

% A = repmat(lind, [rows, cols, 1]); % slower
% label_map = sum(A .* bin_maps, 3);
label_map = sum(bsxfun(@times, bin_maps, lind), 3); % faster

% tries to automatically figure out if bin_map(:,:,1) was an
% object label of interest, or if it was the background
if min(label_map(:)) == 1; label_map = label_map - 1; end;
end
