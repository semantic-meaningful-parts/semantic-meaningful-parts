%----------------------------------------------------------------------------%
% hostname.m
%
% gets hostname of machine you are on
%----------------------------------------------------------------------------%
function name = hostname
[~, name] = unix('hostname');
end