%---------------------------------------------------------------------------
% fusion_opt_global
%
% runs multilabel graph cuts on input given weights and probabilites
% 
% @param img image that we will match the models to produce proposals
% 
%---------------------------------------------------------------------------
function [labels_fused, weights] = fusion_opt_all(atBegining, in_pfg, in_label, ...
  img, yuvb, yuvf, buvb, buvf, gpb, dxi, dyi, pxl_weights, opts)

% parameters
CONSTANT = 1;
UNARY = 1;
PAIR = 0.5;
BOOST = 10;

% pixelweights computation parameters
if ~exist('opts', 'var');
  opts.relative_weights = [0.2, 0.4, 0.4];
  opts.SIGMOID_MEAN = 0.55; opts.SIGMOID_SCALE = 100;
  opts.WEIGHTS_LOW_CUTOFF = 0.1;
  % opts.edge_model.model = edge_model;
end

[rows, cols, chan] = size(img); npx = rows * cols;
weights = [];

%---------------------------------------------------------------------
% unary
%---------------------------------------------------------------------  
nl = size(in_pfg, 3)+1;
% lfg = cat(1, 0, lfg(:));
pbg = max(0.0, 1.0 - sum(in_pfg, 3));

% BETA = 1.5;
% pfg  = 1.0 - exp(-BETA * in_pfg);
pfg  = in_pfg;

punary = 1.0 - cat(3, pbg, pfg);
punary(:,:,2:end) = punary(:,:,2:end) + BOOST*(1.0 - pfg);
punary(:,:,1) = punary(:,:,1) + BOOST*(1.0 - pbg);
unary = reshape(punary, npx, nl)';

%---------------------------------------------------------------------
% compute pairwise pixel weights
%---------------------------------------------------------------------
if ~exist('pxl_weights', 'var') || isempty(pxl_weights);
  ed = 0.75 * (1.0 - gpb);
  [yw_all, ywinds, yw_img, yw_uv, yw_gpb] ...
    = make_cvos_weights_current_frame(atBegining, img, yuvb, yuvf, dxi, dyi, ed, opts);
  [bw_all, bwinds, bw_img, bw_uv, bw_gpb] ...
    = make_cvos_weights_current_frame(atBegining, img, buvb, buvf, dxi, dyi, ed, opts);
  % pxl_weights = yw_all;
  pxl_weights = 0.1*yw_img + 1.0*yw_gpb + 0.1*yw_uv + 15.0*bw_uv;
  % sweights = sigmoid(pxl_weights, opts.SIGMOID_MEAN, opts.SIGMOID_SCALE);
  % fig; imagex([yw_img; yw_gpb; yw_uv; bw_uv]);
end
weights = pxl_weights;
pair = sparse([dxi(:,1); dyi(:,1)], [dxi(:,2); dyi(:,2)], ...
    double(weights(:)), npx, npx);

%---------------------------------------------------------------------
% do optimization
%---------------------------------------------------------------------
cnts = sum(in_label, 3); % how many labels touch each pixel
init_labels = sum(bsxfun(@times, double(in_label), reshape([1:nl-1], [1,1,nl-1])), 3)+1;
init_labels(cnts > 1) = 1;

h = GCO_Create(npx, nl); % global opt for now
GCO_SetDataCost(h, CONSTANT * UNARY * unary); % unary / similarity scores
GCO_SetSmoothCost(h, 1 - eye(nl)); % pott's model on label values
GCO_SetNeighbors(h, CONSTANT * PAIR * pair);

% solve problem
GCO_SetLabeling(h, int32(init_labels(:)));
GCO_SetVerbosity(h, 2);
gco_maxiters = max(rows, cols)*10;
GCO_Expansion(h, gco_maxiters);
L = GCO_GetLabeling(h);
% [E, D, S] = GCO_ComputeEnergy(h);

%--------------------------
% try again if it doesn't converge
% up to 5 times:
%   try 3 runs with 10x more maxiters
%--------------------------
ngcobads = 0;
ngcos = 3;
if (GCO_GetNumIters(h) >= gco_maxiters);
  while ngcobads < 5;
    ngcobads = ngcobads + 1;
    if (GCO_GetNumIters(h) == gco_maxiters);
      E0 = zeros(ngcos,1); D0 = zeros(ngcos,1); S0 = zeros(ngcos,1);
      L0 = cat(3, L, zeros(lrows, lcols, ngcos-1));
      [E0(1), D0(1), S0(1)] = GCO_ComputeEnergy(h);
      
      for (k = 2:ngcos);
        GCO_SetLabeling(h, int32(l_fginter(:))+1);
        gco_maxiters = gco_maxiters + 1;
        GCO_Expansion(h, gco_maxiters);
        L0(:,:,k) = GCO_GetLabeling(h);
        [E0(k), D0(k), S0(k)] = GCO_ComputeEnergy(h);
      end
      
      Estd = std(double(E0));
      if Estd < (10 * ngcobads);
        L = median(L0, 3); break;
      end
      gco_maxiters = gco_maxiters * 10;
    else
      L = GCO_GetLabeling(h);
    end
    
    % just for testing check
    if ngcobads > 3; fprintf('many bad gco runs\n'); keyboard; end;
  end
end
GCO_Delete(h);

labels_fused = reshape(L, rows, cols);
end

