function o = render_model(model0, img, torso_id, a, ids)
if ~exist('ids', 'var')
  ids = 1:length(model0);
end
% just image dimensions
if numel(img) == 2; img = zeros(img); end;
[rows,cols,chan] = size(img); imsize = [rows,cols];
% imagesc(img);
% hold on;
num_labels = length(model0);
if ~exist('torso_id','var'); % v0 representation
  for label_id = 1:num_labels;
    ellipse_model = model0{label_id};
    num_ellipses = numel(ellipse_model); % number of ellipses for part
    for ellipse_id = 1:num_ellipses;
      X0 = ellipse_model(ellipse_id).X0_in;
      Y0 = ellipse_model(ellipse_id).Y0_in;
      phi = ellipse_model(ellipse_id).phi;
      major_axis = ellipse_model(ellipse_id).a;
      minor_axis = ellipse_model(ellipse_id).b;
  
      [x, y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);
      % plot(x, y, 'ko', 'MarkerSize', 3);
      img(sub2ind(imsize, y, x)) = 0;
    end
  end
else % attempted v1 representation
  torso = model0{torso_id}; 
  if ~exist('a','var') || isempty(a); 
    % a = torso.a;
    a = torso.long_axis;
  end

  % draw ellipses in order based on depth
  depths = zeros(1, num_labels);
  for ki = 1:num_labels;
    part = model0{ki};
    d = part(1).Z1;
    for eid = 2:numel(part);
      d = min(d, part(eid).Z1);
    end
    depths(ki) = d;
  end
  [~, idx] = sort(depths, 'ascend');

  for ki = 1:num_labels;
    k = idx(ki);

    part = model0{k};
    num_ellipses = numel(part); % number of ellipses for part
    for eid = 1:num_ellipses;
      X0 = a*part(eid).x1 + torso.X0_in;
      Y0 = a*part(eid).y1 + torso.Y0_in;
      % X0 = a*part(eid).x1 + torso.X0_in;
      % Y0 = a*part(eid).y1 + torso.Y0_in;
      deg = part(eid).phi1;
      if part(eid).axis_flip; deg = mod(deg+90, 360); end;
      phi = deg2rad(deg) + torso.phi;
      % major_axis = a*part(eid).a1;
      % minor_axis = a*part(eid).b1;
      major_axis = a*(part(eid).long / 2);
      minor_axis = a*(part(eid).short / 2);

      if part(eid).axis_flip; % flip the axes
        q=major_axis; major_axis = minor_axis; minor_axis=q;
      end

      try
      [x, y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi, 'fill');
      out_of_bounds = y<1 | y>rows | x<1 | x>cols;
      y(out_of_bounds) = []; x(out_of_bounds) = [];
      if ismember(k, ids)
        img(sub2ind(imsize, y, x)) = k;
      end
      % color = get_jet_color(k, num_labels);
      % plot(x, y, 'Color', color, 'MarkerSize', 3);
      [x, y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi, 'default');
      out_of_bounds = y<1 | y>rows | x<1 | x>cols;
      y(out_of_bounds) = []; x(out_of_bounds) = [];
      img(sub2ind(imsize, y, x)) = 0.5;
      % plot(x, y, 'ko', 'MarkerSize', 3);
      catch e; fprintf('plot ellipse issue\n'); end
    end
  end
end
% imagesc(img);
% hold off;
o = img; % TODO: replace with image data (rgb instedd of a plot handle)
end

% linearly chooses color from jet colormap
function c = get_jet_color(id, max_id);
cm = colormap('jet');
ncolors = size(cm,1);
color_idx = floor(id / max_id * ncolors);
c = cm(color_idx,:);
end
