function m = deg2rotmatrix(deg)
rad = deg2rad(deg);
m = [[ cos(rad), 0, sin(rad)];
     [ 0,        1, 0       ];
     [-sin(rad), 0, cos(rad)]];
end
