% function p(ID)

if ~exist('ID','var'); ID = 3; end; % frontal view of horse
% ID = 1; % side view horse
% ID = 2; % heavy side view horse with tail
% ID = 8; % super tough frontal view
% ID = 15; % looks like tough frontal side-ish view

img = horse_training_images{ID};
msk = horse_training_masks{ID};

q = meaningful_parts(img, msk);


% end


