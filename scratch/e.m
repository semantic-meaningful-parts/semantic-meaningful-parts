% ellipse model for object

tic;
labels_plot = horse_training_labels{2};
[rows,cols,chan] = size(labels_plot); imsize = [rows, cols];
label_ids = setdiff(unique(labels_plot(:)), 0);
num_labels = numel(label_ids);
% Extract the labels
labels = cell(1, num_labels);
for i = 1:num_labels
  label_id = label_ids(i);
  ind_label = find(labels_plot == label_id);
  yx_label = index2yx([1, 1; size(labels_plot)], ind_label);
  labels{label_id} = yx_label;
end
% Define ellipse struct
ellipse_struct = struct('a', [],... // radius of major axis
                        'b', [],... // radius of minor axis
                        'phi', [],... // angle  (w.r.t. image plane)
                        'X0', [],... // center
                        'Y0', [],...
                        'X0_in', [],... // but use this center
                        'Y0_in', [],...
                        'long_axis', [],... // diameter of major axis
                        'short_axis', [],... // diameter of minor axis
                        'status', '');
% Generate ellipses for each label
for label_id = 1:num_labels
  yx_label = labels{label_id};
  ellipse_struct(label_id) = fit_ellipse(yx_label(:, 2), yx_label(:, 1));
end
% Generate best fit single ellipse for each part label
if iambtay; fig(11); else figure; end;
hold on;
for label_id = 1:num_labels
  X0 = ellipse_struct(label_id).X0_in;
  Y0 = ellipse_struct(label_id).Y0_in;
  major_axis = ellipse_struct(label_id).a;
  minor_axis = ellipse_struct(label_id).b;
  phi = ellipse_struct(label_id).phi;
  [x, y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);
  plot(x, y, 'ko', 'MarkerSize', 3);
end
hold off;

% Fit multiple ellipses based on coverage and fit cost
ellipse_models = cell(1, num_labels);
for label_id = 1:num_labels
  % Extra the shape plot of the label
  shape = labels{label_id};
  ellipse_models{label_id} = fit_ellipse_model(shape, size(labels_plot), 3);
end
% Generate the n best fit ellipses for each part labels
if iambtay; fig(12); else figure; end;
imagesc(labels_plot);
hold on;
for label_id = 1:num_labels
  ellipse_model = ellipse_models{label_id};
  num_ellipses = numel(ellipse_model);
  for ellipse_id = 1:num_ellipses
    X0 = ellipse_model(ellipse_id).X0_in;
    Y0 = ellipse_model(ellipse_id).Y0_in;
    major_axis = ellipse_model(ellipse_id).a;
    minor_axis = ellipse_model(ellipse_id).b;
    phi = ellipse_model(ellipse_id).phi;
    [x, y] = plot_ellipse(X0, Y0, major_axis, minor_axis, phi);
    plot(x, y, 'ko', 'MarkerSize', 3);
  end
end
hold off;

toc(tic)


% btay additions
a = ellipse_struct;



torso_id = 2;
torso = ellipse_models{torso_id};

% model0 is an ellipse model of the object, mostly the same as the prior 
% ellipse struct but relative to the torso, allowing easier comparison to
% models from other images
% model0 = ellipse_models;
clear model0;
nparts = length(ellipse_models);
ia = 1.0 / torso.long_axis;
for k = 1:nparts;
  clear part2;
  part = ellipse_models{k}; % part2 = part;
  num_ellipses = numel(part);
  for eid = 1:num_ellipses;
    % sigh: which way a and b are
    long = part(eid).long_axis;
    if abs(long - 2*part(eid).a) < abs(long - 2*part(eid).b);
      part2(eid).axis_flip = false;
    else
      part2(eid).axis_flip = true;
    end

    part2(eid).X1 = ia*(part(eid).X0_in - torso.X0_in);
    part2(eid).Y1 = ia*(part(eid).Y0_in - torso.Y0_in);
    part2(eid).Z1 = 0;
    % part2(eid).a1 = ia * part(eid).a;
    % part2(eid).b1 = ia * part(eid).b;
    part2(eid).long = ia * part(eid).long_axis;
    part2(eid).short = ia * part(eid).short_axis;
    phi0 = part(eid).phi;
    if part2(eid).axis_flip; phi0 = phi0 - pi/2; end;
    part2(eid).phi1 = mod(rad2deg(phi0 - torso.phi), 360);

    part2(eid).Zcam = ia*floor(cols/2);
    % part2(eid).X2 = part2(eid).X1 + part2(eid).a1*cos(phi0);
    % part2(eid).Y2 = part2(eid).Y1 + part2(eid).a1*sin(phi0);
    part2(eid).X2 = part2(eid).X1 + part2(eid).long*cos(phi0);
    part2(eid).Y2 = part2(eid).Y1 + part2(eid).long*sin(phi0);
    part2(eid).Z2 = 0;

    %%%% TODO: USE 3RD point so that we can maintain thickness of rotates about y axis along long axis (in direction of short axis)
    part2(eid).X3 = part2(eid).X1 + part2(eid).short*sin(phi0);
    part2(eid).Y3 = part2(eid).Y1 + part2(eid).short*cos(phi0);
    part2(eid).Z3 = 0;

  end
  model0{k} = part2;
end
model0{torso_id}.a = torso.a;
model0{torso_id}.long_axis = torso.long_axis;
model0{torso_id}.X0_in = torso.X0_in;
model0{torso_id}.Y0_in = torso.Y0_in;
model0{torso_id}.phi = torso.phi;

%%% % from model0, generate novel viewpoints of the object (from ellipses)
%%% % create rotation matrix
%%% deg = 45;
%%% mrotate = deg2rotmatrix(deg);
%%% model0_45 = xform_model(model0, torso_id, mrotate);
%%% img_000 = render_model(model0, labels_plot, torso_id);
%%% img_045 = render_model(model0_45, labels_plot, torso_id);

% draw various rotations in 360 degree circle
nrots = 16;
nrow = nrots/2;
fig(15);
z = zeros(imsize);
for kr = 0:nrots-1;
% for kr = 0:0;
  deg = floor(kr / nrots * 360);
  mrotate = deg2rotmatrix(deg);
  % keyboard;
  model_rot = xform_model(model0, torso_id, mrotate);
  if kr == 0;
    o = render_model(model_rot, labels_plot, torso_id);
  else
    o = render_model(model_rot, z, torso_id);
  end
  fig(15); subplot(2,nrow,kr+1); imagesc(o); title(deg);
end

