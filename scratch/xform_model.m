%-----------------------------------------------------------------------------
% multiples points in the model by the rotation matrix m (could be any matrix
% but we're mainly interested in specific rotations) and spits out an image
%
% @return: model1 : model with adjusted points in X1,Y1, etc. to test model 
%   obtained from the test image with. populate and/or change the following:
%   X1,Y1: transformed center X0_in,Y0_in
%   a1,b1: transformed a,b in new perspective
%   phi1: transformed phi1 in new perspective
% 
%-----------------------------------------------------------------------------
function model1 = xform_model(model0, torso_id, m)

% just to make things look prettier for now
ZCHILL = 100.0;

%--------------------------------------------------------------------
% Hack
%--------------------------------------------------------------------
model1 = model0;
torso = model0{torso_id};
nparts = length(model0);
for k = 1:nparts;
  part = model0{k}; part2 = part;
  num_ellipses = numel(part);
  for eid = 1:num_ellipses;
    zcam = part(eid).Zcam+ZCHILL; 

    % 1. get center
    p = m * [part(eid).X1; part(eid).Y1; part(eid).Z1];
    part2(eid).X1 = p(1); part2(eid).Y1 = p(2); part2(eid).Z1 = p(3);
    zscale = abs(zcam) / abs(p(3)-zcam);
    part2(eid).x1 = zscale * part2(eid).X1;
    part2(eid).y1 = zscale * part2(eid).Y1;

    % 2. get long,short point
    p = m * [part(eid).X2; part(eid).Y2; part(eid).Z2];
    part2(eid).X2 = p(1); part2(eid).Y2 = p(2); part2(eid).Z2 = p(3);
    zscale = abs(zcam) / abs(p(3)-zcam);
    part2(eid).x2 = zscale * part2(eid).X2;
    part2(eid).y2 = zscale * part2(eid).Y2;

    p = m * [part(eid).X3; part(eid).Y3; part(eid).Z3];
    part2(eid).X3 = p(1); part2(eid).Y3 = p(2); part2(eid).Z3 = p(3);
    zscale = abs(zcam) / abs(p(3)-zcam);
    part2(eid).x3 = zscale * part2(eid).X3;
    part2(eid).y3 = zscale * part2(eid).Y3;

    % 3. get new lengths (a,b)
    dy  = part2(eid).y2 - part2(eid).y1;
    dx  = part2(eid).x2 - part2(eid).x1;
    dy3 = part2(eid).y3 - part2(eid).y1;
    dx3 = part2(eid).x3 - part2(eid).x1;

    % part2(eid).a1 = sqrt(dy*dy + dx*dx);
    % part2(eid).b1 = part(eid).b1 * (part2(eid).a1 / part(eid).a1);
    short = sqrt(dy3*dy3 + dx3*dx3);
    long = sqrt(dy*dy + dx*dx);
    long_now = max(long, short);
    part2(eid).long = max(long_now, part(eid).short);
    part2(eid).short = max(short, part(eid).short);
    % part2(eid).short = part(eid).short * (part2(eid).long / part(eid).long);

    % 4. recompute ellipse parameters
    phi_new = mod(rad2deg(atan2(dy,dx)-torso.phi), 360);
    % keyboard;
    part2(eid).phi1 = phi_new;
    % part2(eid).phi1 = part(eid).phi1;

    % 5. keep old stuff
    part2(eid).axis_flip = part(eid).axis_flip;

  end
  model1{k} = part2;
end
model1{torso_id}.a = torso.a;
model1{torso_id}.long_axis = torso.long_axis;
model1{torso_id}.X0_in = torso.X0_in;
model1{torso_id}.Y0_in = torso.Y0_in;
model1{torso_id}.phi = torso.phi;

%%% %--------------------------------------------------------------------
%%% % Proper
%%% %--------------------------------------------------------------------
%%% nparts = length(model0);
%%% for k = 1:nparts;
%%%   part = model0{k};
%%%   num_ellipses = numel(part);
%%%   for eid = 1:num_ellipses
%%% 
%%%     % 1. get center
%%%     p = m * [part.X1; part.Y1; part.Z1];
%%%     part.X1 = p(1);
%%%     part.Y1 = p(2);
%%%     part.Z1 = p(3);
%%% 
%%%     % 2. get new lengths (a,b)
%%%     part.phi1 = mod(rad2deg(ellipse_model0(k).phi - torso.phi), 360);
%%%     part.a1 = ia * ellipse_model(k).a;
%%%     part.b1 = ia * ellipse_model(k).b;
%%%   end
%%%   model0{k} = part;
%%% end
%%% 
%%% % 2. uses z axis to decide which to draw on top / behind (how to order them)

end
