function cell_celldata = matdata2cell(matdata_cells, dim)
  num_classes = numel(matdata_cells);
  % In case the dimensions was not given
  if ~exist('dim', 'var') || isempty(dim)
    matdata = matdata_cells{1};
    A = matdata(1, :);
    dim = sqrt(double(numel(A)));
    dim = [dim, dim];
  end % end-if-!exist
  % For each class, exists a matrix of rows of images
  cell_celldata = cell(1, num_classes);
  for class_id = 1:num_classes
    matdata = matdata_cells{class_id};
    num_imgs = size(matdata, 1);
    celldata = cell(1, num_imgs);
    for img_id = 1:num_imgs
      A = matdata(img_id, :);
      A = reshape(A, dim);
      celldata{img_id} = A';
    end % end-img_id
    if num_classes == 1
      cell_celldata = celldata;
    else
      cell_celldata{class_id} = celldata;
    end
  end % end-class_id
end % end-function