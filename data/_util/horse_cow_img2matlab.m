clear all;
horse_train_images_dir = 'data/horse_cow/horse/train/image/';
horse_train_labels_dir = 'data/horse_cow/horse/train/part/';
horse_test_images_dir = 'data/horse_cow/horse/test/image/';
horse_test_labels_dir = 'data/horse_cow/horse/test/part/';
cow_train_images_dir = 'data/horse_cow/cow/train/image/';
cow_train_labels_dir = 'data/horse_cow/cow/train/part/';
cow_test_images_dir = 'data/horse_cow/cow/test/image/';
cow_test_labels_dir = 'data/horse_cow/cow/test/part/';

SPECIAL_LABELS = true;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For processing HORSE images %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
horse_train_image_files = dir(horse_train_images_dir);
horse_train_label_files = dir(horse_train_labels_dir);
% Remove . .. DS_Store
num_files = numel(horse_train_image_files);
images_retained = [];
for file_idx = 1:num_files
  if ~strcmp(horse_train_image_files(file_idx).name(1), '.')
    images_retained = [images_retained, file_idx];
  end
end
horse_train_image_files = horse_train_image_files(images_retained);
% Remove . .. DS_Store
num_files = numel(horse_train_label_files);
labels_retained = [];
for file_idx = 1:num_files
  if ~strcmp(horse_train_label_files(file_idx).name(1), '.')
    labels_retained = [labels_retained, file_idx];
  end
end
horse_train_label_files = horse_train_label_files(labels_retained);

num_images = numel(horse_train_image_files);
horse_training_images = cell(1, num_images);
horse_training_images_bw = cell(1, num_images);
horse_training_images_seg = cell(1, num_images);
horse_training_images_bw_seg = cell(1, num_images);
horse_training_labels = cell(1, num_images);
horse_training_masks = cell(1, num_images);
% Process the training images
for image_idx = 1:num_images
  image_name = horse_train_image_files(image_idx).name;
  label_name = horse_train_label_files(image_idx).name;
  image_path = strcat(horse_train_images_dir, image_name);
  label_path = strcat(horse_train_labels_dir, label_name);

  img = imread(image_path);
  label_mask = imread(label_path);
  bin_mask = label_mask;
  bin_mask(bin_mask > 0) = 1;
  bin_mask_rgb = repmat(bin_mask, 1, 1, 3);
  seg_img =  img.*bin_mask_rgb;
  bw_seg_img = rgb2gray(seg_img);
  bw_img = rgb2gray(img);

  if SPECIAL_LABELS
    label_mask = special_labels(label_mask, 'animals');
  end

  horse_training_images{image_idx} = img;
  horse_training_images_bw{image_idx} = bw_img;
  horse_training_images_seg{image_idx} = seg_img;
  horse_training_images_bw_seg{image_idx} = bw_seg_img;
  horse_training_labels{image_idx} = label_mask;
  horse_training_masks{image_idx} = bin_mask;

  % skeleton = symmetry_axis(bin_mask);
  % endpoints = bwmorph(skeleton, 'endpoints');
  % branchpoints = bwmorph(skeleton, 'branchpoints'); 
  % keypoints = endpoints+branchpoints;

  % figure;
  % subplot(3, 2, 1); imagesc(img); colorbar;
  % title('Original Image');
  % subplot(3, 2, 2); imagesc(label_mask); colorbar;
  % title('Part Mask');
  % subplot(3, 2, 3); imagesc(seg_img); colorbar;
  % title('Segmented Object');
  % subplot(3, 2, 4); imagesc(bin_mask); colorbar;
  % title('Binary Mask');
  % subplot(3, 2, 5); imagesc(skeleton); colorbar;
  % title('Skeleton');
  % subplot(3, 2, 6); imagesc(keypoints); colorbar;
  % title('Keypoints');
end
% Process the testing images
horse_test_image_files = dir(horse_test_images_dir);
horse_test_label_files = dir(horse_test_labels_dir);
% Remove . .. DS_Store
num_files = numel(horse_test_image_files);
images_retained = [];
for file_idx = 1:num_files
  if ~strcmp(horse_test_image_files(file_idx).name(1), '.')
    images_retained = [images_retained, file_idx];
  end
end
horse_test_image_files = horse_test_image_files(images_retained);
% Remove . .. DS_Store
num_files = numel(horse_test_label_files);
labels_retained = [];
for file_idx = 1:num_files
  if ~strcmp(horse_test_label_files(file_idx).name(1), '.')
    labels_retained = [labels_retained, file_idx];
  end
end
horse_test_label_files = horse_test_label_files(labels_retained);

num_images = numel(horse_test_image_files);
horse_testing_images = cell(1, num_images);
horse_testing_images_bw = cell(1, num_images);
horse_testing_images_seg = cell(1, num_images);
horse_testing_images_bw_seg = cell(1, num_images);
horse_testing_labels = cell(1, num_images);
horse_testing_masks = cell(1, num_images);
% Process the training images
for image_idx = 1:num_images
  image_name = horse_test_image_files(image_idx).name;
  label_name = horse_test_label_files(image_idx).name;
  image_path = strcat(horse_test_images_dir, image_name);
  label_path = strcat(horse_test_labels_dir, label_name);

  img = imread(image_path);
  label_mask = imread(label_path);
  bin_mask = label_mask;
  bin_mask(bin_mask > 0) = 1;
  bin_mask_rgb = repmat(bin_mask, 1, 1, 3);
  seg_img =  img.*bin_mask_rgb;
  bw_seg_img = rgb2gray(seg_img);
  bw_img = rgb2gray(img);

  if SPECIAL_LABELS
    label_mask = special_labels(label_mask, 'animals');
  end

  horse_testing_images{image_idx} = img;
  horse_testing_images_bw{image_idx} = bw_img;
  horse_testing_images_seg{image_idx} = seg_img;
  horse_testing_images_bw_seg{image_idx} = bw_seg_img;
  horse_testing_labels{image_idx} = label_mask;
  horse_testing_masks{image_idx} = bin_mask;

  % skeleton = symmetry_axis(bin_mask);
  % endpoints = bwmorph(skeleton, 'endpoints');
  % branchpoints = bwmorph(skeleton, 'branchpoints'); 
  % keypoints = endpoints+branchpoints;

  % figure;
  % subplot(3, 2, 1); imagesc(img); colorbar;
  % title('Original Image');
  % subplot(3, 2, 2); imagesc(label_mask); colorbar;
  % title('Part Mask');
  % subplot(3, 2, 3); imagesc(seg_img); colorbar;
  % title('Segmented Object');
  % subplot(3, 2, 4); imagesc(bin_mask); colorbar;
  % title('Binary Mask');
  % subplot(3, 2, 5); imagesc(skeleton); colorbar;
  % title('Skeleton');
  % subplot(3, 2, 6); imagesc(keypoints); colorbar;
  % title('Keypoints');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For processing COW images %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cow_train_image_files= dir(cow_train_images_dir);
cow_train_label_files = dir(cow_train_labels_dir);
% Remove . .. DS_Store
num_files = numel(cow_train_image_files);
images_retained = [];
for file_idx = 1:num_files
  if ~strcmp(cow_train_image_files(file_idx).name(1), '.')
    images_retained = [images_retained, file_idx];
  end
end
cow_train_image_files = cow_train_image_files(images_retained);
% Remove . .. DS_Store
num_files = numel(cow_train_label_files);
labels_retained = [];
for file_idx = 1:num_files
  if ~strcmp(cow_train_label_files(file_idx).name(1), '.')
    labels_retained = [labels_retained, file_idx];
  end
end
cow_train_label_files = cow_train_label_files(labels_retained);

num_images = numel(cow_train_image_files);
cow_training_images = cell(1, num_images);
cow_training_images_bw = cell(1, num_images);
cow_training_images_seg = cell(1, num_images);
cow_training_images_bw_seg = cell(1, num_images);
cow_training_labels = cell(1, num_images);
cow_training_masks = cell(1, num_images);
% Process the images
for image_idx = 1:num_images
  image_name = cow_train_image_files(image_idx).name;
  label_name = cow_train_label_files(image_idx).name;
  image_path = strcat(cow_train_images_dir, image_name);
  label_path = strcat(cow_train_labels_dir, label_name);

  img = imread(image_path);
  label_mask = imread(label_path);
  bin_mask = label_mask;
  bin_mask(bin_mask > 0) = 1;
  bin_mask_rgb = repmat(bin_mask, 1, 1, 3);
  seg_img =  img.*bin_mask_rgb;
  bw_seg_img = rgb2gray(seg_img);
  bw_img = rgb2gray(img);

  if SPECIAL_LABELS
    label_mask = special_labels(label_mask, 'animals');
  end

  cow_training_images{image_idx} = img;
  cow_training_images_bw{image_idx} = bw_img;
  cow_training_images_seg{image_idx} = seg_img;
  cow_training_images_bw_seg{image_idx} = bw_seg_img;
  cow_training_labels{image_idx} = label_mask;
  cow_training_masks{image_idx} = bin_mask;

  % skeleton = symmetry_axis(bin_mask);
  % endpoints = bwmorph(skeleton, 'endpoints');
  % branchpoints = bwmorph(skeleton, 'branchpoints'); 
  % keypoints = endpoints+branchpoints;

  % figure;
  % subplot(3, 2, 1); imagesc(img); colorbar;
  % title('Original Image');
  % subplot(3, 2, 2); imagesc(label_mask); colorbar;
  % title('Part Mask');
  % subplot(3, 2, 3); imagesc(seg_img); colorbar;
  % title('Segmented Object');
  % subplot(3, 2, 4); imagesc(bin_mask); colorbar;
  % title('Binary Mask');
  % subplot(3, 2, 5); imagesc(skeleton); colorbar;
  % title('Skeleton');
  % subplot(3, 2, 6); imagesc(keypoints); colorbar;
  % title('Keypoints');
end
% Process the testing images
cow_test_image_files = dir(cow_test_images_dir);
cow_test_label_files = dir(cow_test_labels_dir);
% Remove . .. DS_Store
num_files = numel(cow_test_image_files);
images_retained = [];
for file_idx = 1:num_files
  if ~strcmp(cow_test_image_files(file_idx).name(1), '.')
    images_retained = [images_retained, file_idx];
  end
end
cow_test_image_files = cow_test_image_files(images_retained);
% Remove . .. DS_Store
num_files = numel(cow_test_label_files);
labels_retained = [];
for file_idx = 1:num_files
  if ~strcmp(cow_test_label_files(file_idx).name(1), '.')
    labels_retained = [labels_retained, file_idx];
  end
end
cow_test_label_files = cow_test_label_files(labels_retained);

num_images = numel(cow_test_image_files);
cow_testing_images = cell(1, num_images);
cow_testing_images_bw = cell(1, num_images);
cow_testing_images_seg = cell(1, num_images);
cow_testing_images_bw_seg = cell(1, num_images);
cow_testing_labels = cell(1, num_images);
cow_testing_masks = cell(1, num_images);
% Process the training images
for image_idx = 1:num_images
  image_name = cow_test_image_files(image_idx).name;
  label_name = cow_test_label_files(image_idx).name;
  image_path = strcat(cow_test_images_dir, image_name);
  label_path = strcat(cow_test_labels_dir, label_name);

  img = imread(image_path);
  label_mask = imread(label_path);
  bin_mask = label_mask;
  bin_mask(bin_mask > 0) = 1;
  bin_mask_rgb = repmat(bin_mask, 1, 1, 3);
  seg_img =  img.*bin_mask_rgb;
  bw_seg_img = rgb2gray(seg_img);
  bw_img = rgb2gray(img);

  if SPECIAL_LABELS
    label_mask = special_labels(label_mask, 'animals');
  end

  cow_testing_images{image_idx} = img;
  cow_testing_images_bw{image_idx} = bw_img;
  cow_testing_images_seg{image_idx} = seg_img;
  cow_testing_images_bw_seg{image_idx} = bw_seg_img;
  cow_testing_labels{image_idx} = label_mask;
  cow_testing_masks{image_idx} = bin_mask;

  % skeleton = symmetry_axis(bin_mask);
  % endpoints = bwmorph(skeleton, 'endpoints');
  % branchpoints = bwmorph(skeleton, 'branchpoints'); 
  % keypoints = endpoints+branchpoints;

  % figure;
  % subplot(3, 2, 1); imagesc(img); colorbar;
  % title('Original Image');
  % subplot(3, 2, 2); imagesc(label_mask); colorbar;
  % title('Part Mask');
  % subplot(3, 2, 3); imagesc(seg_img); colorbar;
  % title('Segmented Object');
  % subplot(3, 2, 4); imagesc(bin_mask); colorbar;
  % title('Binary Mask');
  % subplot(3, 2, 5); imagesc(skeleton); colorbar;
  % title('Skeleton');
  % subplot(3, 2, 6); imagesc(keypoints); colorbar;
  % title('Keypoints');
end

% Clean up variables
clear SPECIAL_LABELS;
clear num_images file_idx num_files;
clear image_idx image_name image_path images_retained;
clear skeleton branchpoints endpoints keypoints;
clear label_name label_path labels_retained; 
clear horse_train_images_dir horse_train_labels_dir
clear horse_test_images_dir horse_test_labels_dir;
clear horse_train_image_files horse_train_label_files;
clear horse_test_image_files horse_test_label_files; 
clear cow_train_images_dir cow_train_labels_dir;
clear cow_test_images_dir cow_test_labels_dir;
clear cow_train_image_files cow_train_label_files;
clear cow_test_image_files cow_test_label_files;
clear img label_mask bin_mask bin_mask_rgb bw_img bw_seg_img seg_img; 
