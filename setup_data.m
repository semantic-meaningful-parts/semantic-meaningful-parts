% Horse and Cow dataset
fprintf('Loading HORSES and COWS dataset...\n');
load('data/HORSE_COW');
apply_horse_cow_changes
fprintf('Loading Weizmann Horses dataset...\n');
load('data/weizmann_images_db');

% Clean up workspace
clear cow_testing_images cow_training_images_bw  horse_testing_images_bw_seg
clear cow_testing_images_bw cow_training_images_bw_seg horse_testing_images_seg
clear cow_testing_images_bw_seg cow_training_images_seg horse_testing_labels
clear cow_testing_images_seg cow_training_labels horse_testing_masks
clear cow_testing_labels cow_training_masks
clear cow_testing_masks horse_testing_images horse_testing_images_bw
clear cow_training_images